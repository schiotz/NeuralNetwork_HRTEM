import sys
#sys.path.insert(0, '../../')
#sys.path.insert(0, '../../../NeuralNetwork_HRTEM')
import os
import time
import shutil
import argparse
import numpy as np
import scipy
import json
from ase.io import write
from scipy.cluster.hierarchy import fcluster, linkage
from multiprocessing import Pool
from reproducible import make_reproducible

class SampleMaker:
    """Create samples of 2D materials.

    SampleMaker is a base class, a method called make_data that creates the atoms object
    must be defined.
    """

    save_properties = {}
    
    def __init__(self, distort=None):
        self.distort = distort

    def run(self, first_number, last_number, dir_name):
        for i in range(first_number, last_number): 
            atoms, positions, sites, classes = self.make_data(i)
            # Check that the datatypes are correct
            assert len(positions.shape) == 2
            assert positions.shape[1] == 2
            if sites is not None:
                assert len(sites.shape) == 2
                assert sites.shape[1] == 2
                assert len(classes) == len(sites)
            assert issubclass(positions.dtype.type, float)
            assert issubclass(classes.dtype.type, np.integer)

            np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                        sites=sites,
                        positions=positions,
                        classes=classes
                    )
            write('{0}/model/model_{1:04d}.traj'.format(dir_name,i), atoms)
            if hasattr(self, 'remove_support'):
                atoms_nosupport = self.remove_support(atoms)
                write('{0}/model_no_support/model_no_support_{1:04d}.traj'.format(dir_name,i), atoms_nosupport)

            properties = {k: getattr(self, v) for k, v in self.save_properties.items()}
            if self.save_properties:
                with open('{0}/model_properties/model_properties_{1:04d}.json'.format(dir_name,i), "wt") as pfile:
                    json.dump(properties, pfile, sort_keys=True, indent=4)

            print('iteration', i, flush=True)

    def driver(args):
        self.run(**args)

    def make_data(self, index):
        """Make the atomic system and related data.

        The argument index is the number of the system being made - usually ignored.
        
        Returns (atoms, positions, classes) where
        
        atoms is the atomic system.

        positions is the (x,y) coordinates of the atomic columns

        classes are the classes of the atomic columns, or None if all the same class.
        """
        raise NotImplementedError("The method make_data() must be defined in a subclass")

class SampleMakerTrue2D(SampleMaker):
    """Makes samples of 2D materials where there are never multiple atoms in a column.

    An example of such materials could be graphene or h-BN, but not MoS2
    where there are two sulphur atoms in the same position.
    """
    numclasses = 1   # Just a single kind of atoms
    # classes = {42:0, 16:1}  Mapping from atomic numbers to classes
    
    def make_data(self, _unused):
        atoms = self.make_atoms()

        # Get positions directly from the atoms.
        pos = atoms.get_positions()
        atno = atoms.get_atomic_numbers()
        positions = pos[:,:2]
        if self.numclasses == 1:
            classes = np.zeros(len(atoms), int)
        else:
            classes =  np.array([self.classes[z] for z in atno])
            assert len(positions) == len(classes)
        sites=None
        return atoms, positions, sites, classes

class SampleMakerDistinctColumns(SampleMaker):
    """Makes samples of 2D materials where there can be multiple atoms in a column.

    An example of such materials could MoS2, where there are two sulphur
    atoms in the same position.
    
    Columns are classified according to which atoms are in a column if numclasses > 0
    """
    columndistance = 0.3   # Max horizontal distance between atoms in column, in Å.
    # numclasses = 4
    # classes = {(42,): 0, (16,16): 1, (16,):2}
    # defaultclass = 3     # This class is for any other combination of atomic numbers

    def make_data(self, _unused):
        atoms = self.make_atoms()

        # Create positions of the columns.
        positions = atoms.get_positions()[:,:2]
        z = atoms.get_atomic_numbers()
        clusters = fcluster(linkage(positions), self.columndistance, criterion='distance')
        unique = np.unique(clusters)
        # Now cluster is the column number each atom belongs to, and unique is a list of all columns
        sites = np.zeros((len(unique), 2))    # Positions of the colums
        classes = -np.ones(len(unique), int)  # classes of the columns.  Initialize to -1
        for i, u in enumerate(unique):
            sites[i] = np.mean(positions[clusters==u], axis=0)
            atnos = tuple(sorted(z[clusters==u]))
            try:
                c = self.classes[atnos]
            except KeyError:
                c = self.defaultclass
            classes[i] = c
        return atoms, positions, sites, classes
    
def main(driverfunction, scriptfile, *, graphite=False):
    """Main script driver.  

    Requires two arguments, the driver function and the name of the main script file.
    
    Optional arguments enable extra command line option.  Currently supported:
    graphite
    """    
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'number')
    parser.add_argument("folder", help="The name of the folder (in ../../simulation_data) where the output is placed.")
    parser.add_argument("number", type=int,
                        help="The desired number of training examples (incl. any that are already done).")
    parser.add_argument("--size", type=float, default=80.0,
                        help="Maximal system size in Å (default: 80 Å)")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.set_defaults(test=False)
    parser.add_argument("--distort", default=None, type=float,
                        help="Typical amount of distortion added to atomic positions (default: None)")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    if graphite:
        parser.add_argument('--graphite', default=1, type=int,
                            help="Use up to this number of graphite layers for support.")
    args = parser.parse_args()
    
    if args.numproc > args.number:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        numproc = len(os.sched_getaffinity(0))

    dir_name = os.path.join('../../', 'simulation_data', args.folder)
    
    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        0, args.number, dir_name, numproc))

    seed = np.random.SeedSequence(args.seed)
    print("np.random.SeedSequence entropy:", seed.entropy)

    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['model', 'model_no_support', 'points', 'model_properties']:
            os.makedirs(os.path.join(dir_name, subf))

    # Keep a copy of this script for reference
    shutil.copy2(__file__, dir_name)
    if isinstance(scriptfile, str):
        shutil.copy2(scriptfile, dir_name)
    else:
        # Assume list or tuple of file names
        for s in scriptfile:
            shutil.copy2(s, dir_name)
            
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce_structures.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    
    before = time.time()
    
    extraargs = ()
    if graphite:
        extraargs += (args.graphite,)
    if numproc == 1:
        # Running on a single core.
        driverfunction(0, args.number, dir_name, args.size,
                           args.distort, seed, *extraargs)
    else:
        data = []
        ndata = args.number
        seeds = seed.spawn(numproc)
        for i in range(numproc):
            data.append((
                i * ndata // numproc,
                (i+1) * ndata // numproc,
                dir_name,
                args.size,
                args.distort,
                seeds[i],
                ) + extraargs)
        with Pool(numproc) as pool:
            pool.starmap(driverfunction, data)

    print("Time to simulate models: {:.2f} hours on {:d} cores.".format(
        (time.time() - before)/3600, numproc))
