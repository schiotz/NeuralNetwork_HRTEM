import os
import glob
import numpy as np
import cupy as cp
import scipy as sp
import json

from cupyx.scipy import ndimage as cp_ndi
#########################################################
################## image processing #####################
#########################################################
def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    channels = image.shape[-1]
    if channels > 1:
        B=cp.zeros_like(image[:,:,0])
        S=cp.zeros_like(image[:,:,0])
        for i in range(channels):
            B += cp_ndi.gaussian_filter(image[:,:,i],
                                        (sigma1,sigma1),
                                        mode='reflect')
        for i in range(channels):
            image[:,:,i] = image[:,:,i] - B/channels
        
        for i in range(channels):
            S += cp.sqrt(cp_ndi.gaussian_filter(image[:,:,i]**2,
                                                (sigma2,sigma2),
                                                mode='reflect'))
        for i in range(channels):
            image[:,:,i] = image[:,:,i] / (S/channels)
    else:
        image[:,:,0] = image[:,:,0] - cp_ndi.gaussian_filter(image[:,:,0],
                                                             (sigma1,sigma1),
                                                             mode='reflect')
        image[:,:,0] = image[:,:,0] / cp.sqrt(cp_ndi.gaussian_filter(image[:,:,0]**2,
                                                                     (sigma2,sigma2),
                                                                      mode='reflect'))   
    return image.get()

datf = '/home/niflheim2/mhlla/hrid/simulation_data/Au_fcc_classification' 
vdatf = '/home/niflheim2/mhlla/hrid/simulation_data/Au_fcc_classification-test'

## FFT cutoff radius
with open(os.path.join(datf, 'parameters.json')) as json_file:
    par = json.load(json_file)
sampling = par['sampling']
image_epochs = par['image_epochs']
images_per_epoch = par['images_per_epoch']
normalisation_distance = par['normalizedistance']
img_size = par['image_size']

lc = 4.06
lc_px = lc/np.mean(sampling)

w, h = par['image_size']

a = np.arange(w) - w//2
a = len(a) * (2 * np.pi) / (lc_px)
print(f'Fourier cut-off: {a} pixels')

x1 = int(w//2 - a)
x2 = int(w//2 + a)
y1 = int(h//2 - a)
y2 = int(h//2 + a)
print(x1, x2, y1, y2)
dummy = np.zeros((int(x2-x1), int(y2-y1)),dtype=np.uint8)
print(dummy.shape)

## Training data
input_data_dir = datf + '/images_labels'
data_paths = sorted(
    [
        os.path.join(input_data_dir, fname)
        for fname in os.listdir(input_data_dir)
        if fname.endswith(".npz")
        ])
input_params_dir = datf + '/tem_params'
params_paths = sorted(
    [
        os.path.join(input_params_dir, fname)
        for fname in os.listdir(input_params_dir)
        ])
if not os.path.exists(datf + '/ffts'):
    os.makedirs(datf + '/ffts')
for i, (data_path, params_path) in enumerate(zip(data_paths, params_paths)):
    print(data_path)
    print(params_path)
    image_epoch = i%image_epochs
    image_no = i%images_per_epoch
    with open(params_path) as json_file:
        p = json.load(json_file)
    sigma = normalisation_distance/p['sampling']
    data = np.load(data_path)
    
    img = data['image'][:,:img_size[0],:img_size[1],:]
    img = cp_local_normalise(img[0,:,:,:], sigma, sigma)
     
    mask = data['label'][:,:img_size[0],:img_size[1],:]
    
    mask_cp = cp.array(mask[0,:,:,0].copy())
    mask = cp_ndi.gaussian_filter(mask_cp,
                                  (10, 10),
                                  mode='reflect').get()

    img = img[:,:,0]*mask

    f = sp.fft.fftshift(sp.fft.fft2(img))
    f_abs = np.abs(f)
    f = (f_abs-np.min(f_abs))/(np.max(f_abs)-np.min(f_abs))
    f = f[x1:x2, y1:y2]
    print(datf+f'/ffts/fft_{image_epoch:03d}_{image_no:04d}')
    np.save(datf+f'/ffts/fft_{image_epoch:03d}_{image_no:04d}',f) 

## Validation data
input_data_dir = vdatf + '/images_labels'
data_paths = sorted(
    [
        os.path.join(input_data_dir, fname)
        for fname in os.listdir(input_data_dir)
        if fname.endswith(".npz")
        ])
input_params_dir = vdatf + '/tem_params'
params_paths = sorted(
    [
        os.path.join(input_params_dir, fname)
        for fname in os.listdir(input_params_dir)
        ])
if not os.path.exists(vdatf + '/ffts'):
    os.makedirs(vdatf + '/ffts')
for i, (data_path, params_path) in enumerate(zip(data_paths, params_paths)):
    print(data_path)
    print(params_path)
    image_epoch = i%image_epochs
    image_no = i%images_per_epoch
    with open(params_path) as json_file:
        p = json.load(json_file)
    sigma = normalisation_distance/p['sampling']
    data = np.load(data_path)
    
    img = data['image'][:,:img_size[0],:img_size[1],:]
    img = cp_local_normalise(img[0,:,:,:], sigma, sigma)
     
    mask = data['label'][:,:img_size[0],:img_size[1],:]
    
    mask_cp = cp.array(mask[0,:,:,0].copy())
    mask = cp_ndi.gaussian_filter(mask_cp,
                                  (10, 10),
                                  mode='reflect').get()
    img = img[:,:,0]*mask

    f = sp.fft.fftshift(sp.fft.fft2(img))
    f_abs = np.abs(f)
    f = (f_abs-np.min(f_abs))/(np.max(f_abs)-np.min(f_abs))
    f = f[x1:x2, y1:y2]
    print(datf+f'/ffts/fft_{image_epoch:03d}_{image_no:04d}')
    np.save(datf+f'/ffts/fft_{image_epoch:03d}_{image_no:04d}',f) 
