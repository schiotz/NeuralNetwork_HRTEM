import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import numpy as np
#import fft module for fourier filtering
from scipy import fft

try:
    import cupy as cp
except ImportError:
    xp = np
else:
    xp = cp
import scipy
from scipy.constants import value, hbar, Boltzmann
import json
import os
import sys
import shutil
import argparse
import multiprocessing
from pathlib import Path
import time
import ase.io
import abtem
from abtem.mtf import MTF
#from abtem.measure import calibrations_from_grid, Measurement
import dask
from temnn.data.labels import create_label
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from peaks import find_local_peaks


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Path to directory with structures.")
    parser.add_argument("param_file", help="Parameters file.  Optional for test data (reused from training).",
                            nargs='?', default=None)
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument("-n", "--numproc", type=int, default=None,
                        help="Number of processes to use (CPU cores).  Default: Read from parameter file.")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator.  Overrrules seed in parameter JSON file.")
    parser.add_argument('--gpu', action='store_true', help='Run on GPU.')
    parser.set_defaults(test=False)
    args = parser.parse_args()

    # Set abTEM/cuda to use a single worker with a single thread.
    dask.config.set({"num_workers": 1})
    abtem.config.set({"local_diagnostics.progress_bar": False})
    
    ### *** Output Directories name ***
    data_dir =  args.data_dir
    if args.test:
        if args.data_dir.endswith('/'):
            data_dir = Path(args.data_dir[:-1] + '-test')
        else:
            data_dir = Path(args.data_dir + '-test')
    else:
        data_dir = Path(args.data_dir)

    ### Parameter file
    if args.param_file is not None:
        # Explicit parameter file given
        param_file_name = args.param_file
    else:
        if args.test:
            # Get parameter file from training data run
            param_file_name = os.path.join(args.data_dir, 'parameters.json')
            print("Reusing parameter file from training set")
        else:
            print("ERROR: You must specify a parameter file when generating the training set")
            sys.exit(1)
    print("Parameter file:", param_file_name)
    with open(param_file_name, 'r') as pfile:
        parameters = json.load(pfile)

        
    ### Create output directories
    if not data_dir.is_dir():
        data_dir.mkdir(parents=True)
    wave_dir = data_dir / 'wave'
    if not wave_dir.is_dir():
        wave_dir.mkdir()
    wave_ns_dir = data_dir / 'wave_no_support'
    if parameters['label'] == 'Exitwave_no_support' and not wave_ns_dir.is_dir():
        wave_ns_dir.mkdir()
    image_dir = data_dir / 'images_labels'
    if not image_dir.is_dir():
        image_dir.mkdir()
    params_dir = data_dir / 'tem_params'
    if not params_dir.is_dir():
        params_dir.mkdir()
    debug_dir = data_dir / 'debug'
    if not debug_dir.is_dir():
        debug_dir.mkdir()
    
    # Add parameters dictonary elements
    parameters['data_dir'] = str(data_dir)
    parameters['debug_dir'] = str(debug_dir)  # Make it accessible to MakeImages

    # Number of processors.
    # Command line overrides parameter file
    if args.numproc is not None:
        numproc = args.numproc
        print(f'Running on {numproc} cores - given on command line.')
    elif 'numproc' in parameters:
        numproc = parameters['numproc']
        print(f'Running on {numproc} cores - given in parameter file.')
    else:
        try:
            numproc = len(os.sched_getaffinity(0))
        except AttributeError:
            numproc = multiprocessing.cpu_count()
        print(f'Running on all available ({numproc}) cores.')
    if args.gpu:
        numproc = 1
        abtem.config.set({"device": "gpu"});
        print("Running on GPU - use only a single CPU (overruling above).")
    parameters['numproc'] = numproc
    
    # We generate different images for each epoch.  It is not necessary to
    # use all image_epochs when training, and it is possible to reuse
    # them.  In test data, this is overwritten to 1.
    if args.test:
        image_epochs = 1
        parameters['image_epochs'] = image_epochs
    else:
        image_epochs = parameters['image_epochs'] 
    # Extract image size
    sampling = np.mean(parameters['sampling'])
    
    # Find number of input structures, and check that everything looks OK
    n = len(list((data_dir / 'model').iterdir()))
    check_sane_input(data_dir, n)
    
    print("Number of images {} x {} image epochs.".format(
            n, image_epochs), "- Total images: ", n*image_epochs)
    parameters['images_per_epoch'] = n

    # For reproducibility, the seed for the RNG can come from (priorities)
    # 1) The command line.
    # 2) The parameter JSON file
    # 3) Randomly generated
    if args.seed:
        print("Using random seed from command line")
        masterseed = np.random.SeedSequence(args.seed)
    elif 'seed' in parameters:
        print("Using random seed from JSON parameter file")
        masterseed = np.random.SeedSequence(parameters['seed'])
    else:
        print("Generating a new random seed.")
        masterseed = np.random.SeedSequence(None)
    parameters['seed'] = masterseed.entropy
    print("Random seed:", masterseed.entropy)

    # Make a sequence of two seeds, use the first if training, the second if testing.
    actualseed = masterseed.spawn(2)[args.test]

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, data_dir)

    # Also store the parameters in a machine_readable file
    with open(os.path.join(data_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)
 

    ### *** Generate image stream *** 
    make_waves_and_images(data_dir, n, numproc, parameters, actualseed)
        
    ## *** Output ***

def check_sane_input(data_dir, n):
    """Check that the input files are as expected"""
    modeldir = data_dir / 'model'
    pointdir = data_dir / 'points'
    for i in range(n):
        assert (modeldir / f'model_{i:04}.traj').is_file()
        assert (pointdir / f'points_{i:04}.npz').is_file()
        
def make_waves_and_images(data_dir, n, maxcpu, parameters, actualseed):
    """Make the images in parallel."""
    if maxcpu == 1:
        print("Running on a single CPU.")
        make_waves_images_batch(0, n, data_dir, parameters, actualseed)
    else:
        seeds = actualseed.spawn(maxcpu)
        processes = []
        for i in range(maxcpu):
            kwargs = dict(
                start = i * n // maxcpu,
                end = (i + 1) * n // maxcpu,
                data_dir = data_dir,
                parameters = parameters,
                seed = seeds[i],
                )
            print(f'Starting process {i} of {maxcpu} - systems {kwargs["start"]} to {kwargs["end"]-1}.')
            p = multiprocessing.Process(target=make_waves_images_batch, kwargs=kwargs)
            p.start()
            processes.append(p)
        for i, p in enumerate(processes):
            p.join()
            print(f"Stopped process {i}.")


def get_phonon_dict(atoms, debyeTemp, Temp):
    
    std_dict = {}
    u_to_kg = value('atomic mass unit-kilogram relationship')
    
    for atom in atoms:
        symbol = atom.symbol 
        
        if symbol not in std_dict:
            m = atom.mass * u_to_kg
        
            S = np.sqrt(
                (3*hbar**2)/(2*m*Boltzmann*debyeTemp)*(1/4 + Temp / debyeTemp)
            )
            std_dict[symbol] = S * 1e10 # Angstrom

    return std_dict

def make_waves_images_batch(start, end, data_dir, parameters, seed):
    print(f'seed = {seed}')
    sampling = parameters['wave_sampling']
    wave_size = parameters['wave_size']
    debug = parameters['debug']
    debug_dir = Path(parameters['debug_dir'])
    debyewaller = parameters.get('debyewaller', None)
    no_support = parameters['label'] == 'Exitwave_no_support'

    try:
        Phonon_Frozen = (parameters['Phonon'] == 'Frozen') # Bool
    except KeyError:
        Phonon_Frozen = False
    if Phonon_Frozen:
        debyeTemp = parameters['Phonon_Debye_Temp'] # int
        Phonon_Temp = parameters['Phonon_Temp'] # Range
        Phonon_config = parameters['Frozen_Phonon_Configs'] # Int
        Phonon_DebyeWaller = (parameters['Phonon'] == 'DebyeWaller') # Bool
    
    rng = np.random.default_rng(seed)
    phononseeds = rng.integers(int(2**30), size=2)    
    
    for i in range(start, end):
        structure = ase.io.read(data_dir / 'model' / f'model_{i:04}.traj')
        labeldata = np.load(data_dir / 'points' / f'points_{i:04}.npz')
        if no_support:
            structure_nosupport = ase.io.read(data_dir / 'model_no_support' / f'model_no_support_{i:04}.traj')
        
        # The system size should match the wave size
        origcell = structure.cell.diagonal()
        syssize = sampling * np.array(wave_size)
        for j in (0,1):
            structure.cell[j,j] = syssize[j]
            if no_support:
                structure_nosupport.cell[j,j] = syssize[j]
        rmax = structure.get_positions().max(axis=0)
        if rmax[0] > syssize[0] or rmax[1] > syssize[1]:
            raise ValueError("System too big: {rmax} too big for {syssize} (original cell: {origcell})")
        
        if Phonon_Frozen:
            Temp = rng.uniform(*Phonon_Temp)
            material_dict = get_phonon_dict(structure, debyeTemp, Temp)
            print(Temp)
            print(material_dict)
            structure = abtem.FrozenPhonons(structure, Phonon_config, material_dict, seed=phononseeds[0])
            if no_support:
                structure_nosupport = abtem.FrozenPhonons(structure_nosupport, Phonon_config, material_dict, seed=phononseeds[1])

        wave = make_exit_wave(i, structure, wave_size, parameters['multislicethickness'],
                                  parameters['beamenergy'], debyewaller)
        #outfile = data_dir / 'wave' / f'wave_{i:04}'
        #wave.write(outfile)
        if no_support:
            wave_nosupport = make_exit_wave(i, structure_nosupport, wave_size, parameters['multislicethickness'],
                                  parameters['beamenergy'], debyewaller)
            #outfile_ns = data_dir / 'wave_no_support' / f'wave_{i:04}'
            #wave_nosupport.write(outfile_ns)
            #print("Exit wave saved to:", outfile, 'and', outfile_ns)
        else:
            wave_nosupport = None
            #print("Exit wave saved to:", outfile)
        for epoch in range(parameters['image_epochs']):
            image, label, tem_params = makeimage(wave, wave_nosupport, labeldata, parameters, rng)
            ## Save information relevant for vibrations
            if Phonon_Frozen:
                tem_params['Temp'] = Temp
                tem_params['debyeTemp'] = debyeTemp
                tem_params['material_std'] = material_dict
            imagefile = data_dir / 'images_labels' / f'image_label_{epoch:03}_{i:04}.npz'
            np.savez_compressed(imagefile,
                        image=np.asarray(image).astype('float32'),
                        label=np.asarray(label).astype('float32'))
            tem_parfile = data_dir / 'tem_params' / f'parameters_{epoch:03}_{i:04}.json'
            with open(tem_parfile, "wt") as pfile:
                json.dump(tem_params, pfile, sort_keys=True, indent=4)
            #if i % 100 == 0 and epoch == 0:
            if True:
                print('Image and labels saved as ', imagefile, flush=True)
            if epoch == 0 and (debug is True or i < debug):
                fn = debug_dir / f'img-{i:04}.png'
                show_example(image, label, fn, parameters['label'])

def make_exit_wave(i, atoms, wave_size, multislicethickness, beamenergy, debyewaller):
    "Make exit wave from ASE structure"
    potential = abtem.Potential(atoms,
                                gpts=wave_size,
                                parametrization='kirkland',
                                slice_thickness=multislicethickness,
                                projection='finite')
    if debyewaller:
        # Apply Gaussian smearing of the potential as a primitive model for
        # atomic vibrations.  Often referred to as a Debye-Waller factor,
        # although that technically refers to scattering.
        potential = potential.compute(pbar=False)  #Precompute it
        slicethickness = np.mean(potential.slice_thicknesses)
        assert np.abs(potential.slice_thicknesses - slicethickness).max() < 1e9
        gridsampling = (slicethickness,) + potential.sampling
        sigma = np.sqrt(debyewaller / 3.0) / np.array(gridsampling)
        smoothpot = scipy.ndimage.gaussian_filter(potential.array, sigma, mode='wrap')
        potential.array[:] = smoothpot
    wave = abtem.PlaneWave(energy=beamenergy)
    exit_wave=wave.multislice(potential)
    exit_wave.compute()
    return exit_wave


def makeimage(wave, wave_ns, labeldata, params, rng):
    """
    Make a TEM image.

    wave: The wave function used to generate the images

    wave_ns: Wave function for generating ground truth, or None if not applicable.

    labeldata: dict (or dict-like object) with data for ground truth (from points file).

    params: Simulation parameters

    rng: Random number generator
    """
    #######################################################
    ### *** Generate a set of random TEM parameters *** ###
    #######################################################
    tem_params = tem_parameters(params, rng)

    ##############################
    ### *** Extract values *** ###
    ##############################
    ######################
    ## Image parameters ##
    ######################
    # Image dimensions
    size = params['image_size']
    # Image sampling
    sampling = tem_params['sampling']
   
    ######################
    ## Label parameters ##
    ######################
    # Size of spots for atomic column labels - normally 0.4
    spotsize = params['spotsize']
 
    ###########################
    ## Microscope parameters ##
    ###########################
    # Contrast transfer function values
    ctf_params = {
        'C30': tem_params['ctf_c30'],
        'C12': tem_params['ctf_c12'], 'phi12': tem_params['ctf_phi12'],
        'C21': tem_params['ctf_c21'], 'phi21': tem_params['ctf_phi21'],
        'C23': tem_params['ctf_c23'], 'phi23': tem_params['ctf_phi23'],
        'C32': tem_params['ctf_c32'], 'phi32': tem_params['ctf_phi32'],
        'C34': tem_params['ctf_c34'], 'phi34': tem_params['ctf_phi34'],
        'C45': tem_params['ctf_c45'], 'phi45': tem_params['ctf_phi45'] 
    }
    # Focal spread
    focal_spread = tem_params['focal_spread']
    # Electron dose
    dose = tem_params['dose']
    # Gaussian blur
    blur = tem_params['blur']
    # Modulation transfer function values
    mtf_type = tem_params['mtf']
    mtf_params = {f'c{i}' : tem_params[f'mtf_c{i}'] for i in range(4) if f'mtf_c{i}' in tem_params}
    readout_noise = tem_params['readout']
    # Generate a series of images at different focus settings
    if params.get('multifocus') is None:
        multifocus = False
        defocuses = [tem_params['defocus'],]
    else:
        multifocus, deltafocus, deltadeltafocus = params['multifocus']
        defocuses = [tem_params['defocus'],]
        for i in range(1, multifocus):
            defocuses.append(defocuses[-1] + rng.uniform(deltafocus-deltadeltafocus,
                                                deltafocus+deltadeltafocus))
        tem_params['defocus'] = defocuses
 
    ## Debugging options
    # Ouput examples to debug folder
    debug = params['debug']
    debug_dir = params['debug_dir']
    
    #########################################
    ### *** Create Images and Labels *** ####
    #########################################
    # Load waves, atom sites, atom positions, and atom objects as
    # Dataset object
    tem_params['energy'] = wave.energy

    # Initialise CTF and generate image
    for i, defocus in enumerate(defocuses):
        #ctf_params['C10'] = -defocus
        ctf = abtem.CTF(defocus=defocus,
                        energy=wave.energy,
                        focal_spread=focal_spread,
                        **ctf_params
        )
        _image = create_single_image(wave,
                           ctf,
                           sampling,
                           blur,
                           dose,
                           mtf=mtf_type,
                           mtf_params=mtf_params,
                           readout=readout_noise
        )
        if 'FFT_filter_lat_const' in params:
            # If FFT filtering is specified in the parameter file, identify
            # hexagonal peaks with the specified lattice constant, and remove
            # peaks with the three lowest order.
            latconst = params['FFT_filter_lat_const']
            filtered_image = hexagonal_fourier_filter(_image[0,:,:,0], latconst, sampling)
            _image[0,:,:,0] = filtered_image     

        if i > 0:
            image = np.concatenate((image, _image), axis=3)
        else:
            image = _image

    
    ##############################
    ###### Generate label ########
    ##############################
    # We can do Nanoparticle segmentation with Masks or blobs (pseudo-object localisaiton)
    # or we can do atomic segmetnation with discs or gaussian peaks. Masks do not require a width,
    # and the blob with will be calculated based on positions.
    labeltype = params['label']
    num_classes = params['num_classes']
    null_class = params.get('null_class', None)
    spotsize = params.get('spotsize', 0.4)
    imgshape = image.shape[1:-1]

    if labeltype == 'Mask':
        positions = labeldata['positions']
        if isinstance(positions, list):
            # Multi-class labeling, e.g. substrate and nanoparticle
            _positions = [x / sampling for x in positions]
        else:
            _positions = positions / sampling
        labels = create_label(_positions, shape=imgshape, label=labeltype, null_class=null_class,
                                  num_classes=num_classes)
    elif labeltype == 'Blob':
        # Gaussian blob object localisation of nanoparticles
        # Fix Gaussian blob at centroid of points
        positions = labeldata['positions']
        points = positions[:,:2]/sampling
        x = [p[0] for p in points]
        y = [p[1] for p in points]
        centroid = (sum(x) / len(x), sum(y) / len(y))

        # Gaussian width
        distances = [ np.abs(np.sqrt((p[0]-centroid[0])**2 + (p[1]-centroid[1])**2)) for p in points]
        w = np.ceil(max(distances)).astype(int) / 2

        labels = create_label(positions/sampling, shape=imgshape, width=w, label=labeltype,
                                  num_classes=num_classes)
        
    elif labeltype == 'Exitwave' or labeltype == 'Exitwave_no_support':
        if labeltype == 'Exitwave_no_support':
            labels = wave_ns.complex_images().interpolate(sampling)
        else:
            labels = wave.complex_images().interpolate(sampling)
        if len(labels.shape) == 3:
            labels = labels.mean(0)
        labels.compute()
        if isinstance(labels.array, np.ndarray):
            cplx_labels = labels.array
        else:
            cplx_labels = cp.asnumpy(labels.array)
        assert cplx_labels.shape == image.shape[1:3]
        # Convert to separate real and imaginary parts, and subtract 1.0 from real part
        labels = np.empty(cplx_labels.shape + (2,), dtype=np.float32)
        labels[:,:,0] = cplx_labels.real - 1.0
        labels[:,:,1] = cplx_labels.imag

    else:
        assert labeltype == 'Disk' or labeltype == 'Gaussian'
        if not num_classes:   # Explicit label meaning no classes.
            positions = labeldata['positions']
            labels = create_label(positions[:,:2]/sampling,
                                      shape=imgshape,
                                      width=int(spotsize/sampling),
                                      label=labeltype)
        else:
            sites = labeldata['sites']
            try:
                classes = labeldata['classes']
            except KeyError:
                classes = None
            labels = create_label(sites[:,:2]/sampling,
                                      shape=imgshape,
                                      width=int(spotsize/sampling),
                                      label=labeltype,
                                      classes=classes,
                                      null_class=null_class,
                                      num_classes=num_classes)
    labels.shape = (1,) + labels.shape
    
    # Image dimensions 
    shape = image.shape[1:3]
    assert not ((size[0] > shape[0]) != (size[1] > shape[1]))
    if shape[0] > size[1]:
        # Image too big - crop it
        xamount = shape[0] - size[0]
        yamount = shape[1] - size[1]
        assert yamount > 0
        x1 = xamount // 2
        x2 = xamount - x1
        y1 = yamount // 2
        y2 = yamount - y1
        image = image[:,x1:-x2,y1:-y2,:]
        labels = labels[:,x1:-x2,y1:-y2,:]
    elif shape[0] < size[1]:
        # Image too small - pad it
        padding = ((0,0),
                   (0,size[0]-shape[0]),
                   (0,size[1]-shape[1]),
                   (0,0))
        image = np.pad(image, padding, 'constant', constant_values=0)
        labels = np.pad(labels, padding, 'constant', constant_values=0)
    else:
        assert shape[1] == size[1]

    return image, labels, tem_params


def create_single_image(wave, ctf, sampling, blur, dose, mtf=None, mtf_params=None, readout=None):

    image_wave = wave.apply_ctf(ctf)
    # Intensity of wavefunction at image plane
    # Average over frozen phonon axis, if present
    if len(image_wave.shape) == 3:
        measurement_raw = image_wave.intensity().mean(0)
    else:
        assert len(image_wave.shape) == 2
        measurement_raw = image_wave.intensity()
    # Add Gaussian blur
    measurement_blur = measurement_raw.gaussian_filter([blur,blur])
    # Resample to match detector resolution (experimental image resolution)
    measurement = measurement_blur.interpolate(sampling)
    # Add Poisson noise
    measurement_noise = measurement.poisson_noise(dose)
    # Apply MTF
    if mtf is not None:
        if mtf_params is None:
            raise TypeError('mtf type specified, but no parameters')

        if mtf == 'lorentzian': # or MTF == 'lorentzian_correlated' or MTF == 'lorentzian_extra_correlated':
            mtf_func = mtf_lorentzian
        elif mtf == 'exponential':
            mtf_func = mtf_exponential
        elif mtf == 'gaussian':
            mtf_func = mtf_gaussian
        elif mtf == 'parametric':
            mtf_func = mtf_parametric
        else:
            raise ValueError(f'MTF type "{MTF}" is not supported')
        measurement_noise.compute()
        img_array = measurement_noise.array
        measurement_MTF = measurement_noise.copy()
        #s = self._wave.sampling
        s = (sampling,sampling)
        kx = xp.fft.fftfreq(img_array.shape[0], d=s[0])*2*s[0]
        ky = xp.fft.fftfreq(img_array.shape[1], d=s[1])*2*s[1]
        Kx, Ky, = xp.meshgrid(kx, ky, indexing='ij')
        K = xp.sqrt(Kx**2 + Ky**2)
        new_img_array = xp.fft.ifft2(np.fft.fft2(img_array) * mtf_func(K,**mtf_params))
        measurement_MTF.array[:] = new_img_array.real
    else:
        if mtf_params is not None:
            raise TypeError('mtf parameters specified, but no type')
        measurement_noise.compute()
        measurement_MTF = measurement_noise

    if readout is not None:
        N0 = readout
        img_array = measurement_MTF.array
        noise_mask = xp.random.poisson(xp.full((img_array.shape[0], img_array.shape[1]), N0))
        img_array += noise_mask
        measurement_MTF.array[:] = img_array

    image = measurement_MTF.array
    image = image.reshape((1,)+image.shape+(1,)).astype(np.float32)
    if isinstance(image, np.ndarray):
        return image
    else:
        # Assume it is a cupy array, convert to numpy
        return cp.asnumpy(image)

def mtf_lorentzian(q, c1, c2):
    """ 
    Lorentzian MTF scaled so q=q/2qn where qn is the Nyqvist freq.
    
    This is equivalent to q*s where s is the sampling.
    """
    return (1 - c2) * (1/(1 + (q/c1)**2)) + c2

def mtf_parametric(q, c1, c2, c3):
    """ 
    Fully parametric MTF scaled so q=q/2qn where qn is the Nyqvist freq.
    
    This is equivalent to q*s where s is the sampling.
    """
    return (1 - c2) * (1/(1 + (q/c1)**c3)) + c2

def mtf_exponential(q, c1, c2):
    """ 
    Exponential MTF scaled so q=q/2qn where qn is the Nyqvist freq.
    
    This is equivalent to q*s where s is the sampling.
    """
    return (1 - c2) * np.exp(-2*c1*q) + c2

def mtf_gaussian(q, c1, c2):
    """ 
    Gaussian MTF scaled so q=q/2qn where qn is the Nyqvist freq.
    
    This is equivalent to q*s where s is the sampling.
    """
    return (1 - c2) * np.exp(-q**2 / (2*c1**2) ) + c2



def show_example(image, label, filename, labeltype):
    """
    Save a png with image and label
    
    Parameters:
    -----------
    image: numpy array
        The generated TEM image
    label:  numpy array
        The geneated labels (ground truth)
    filename: string
        Output path
    """
    channels = image.shape[-1]
    out_channels = label.shape[-1]
    width = 100.0
    fig = plt.figure(figsize=(width,width/(channels+out_channels)))

    for i in range(channels):
        ax = fig.add_subplot(1, channels+out_channels, i+1)

        im = ax.imshow(image[0,:,:,i].T, origin='lower', interpolation='nearest', cmap='gray')
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax = cax1)
    for j in range(out_channels):
        ax = fig.add_subplot(1, channels+out_channels, channels+j+1)

        if labeltype == "Exitwave" or labeltype == "Exitwave_no_support":
            vmax = max(-label[0].min(), label[0].max())
            extraargs = dict(cmap="seismic", vmin=-vmax, vmax=vmax)
        else:
            extraargs = {}
        im = ax.imshow(label[0,:,:,j].T, origin='lower', interpolation='nearest', **extraargs)
        divider = make_axes_locatable(ax)
        cax2 = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax = cax2)

    plt.tight_layout()
    fig.savefig(filename, bbox_inches='tight')
    plt.close(fig)

    

def tem_parameters(params,rng):
    """
        Genarates the random TEM imaging parameters.
    """

    tem_params = {
        'sampling': rng.uniform(*params['sampling']),
        ## CTF parameters
        'defocus': rng.uniform(*params['defocus']),
        'ctf_c30': rng.uniform(*params['ctf_c30']),
        'ctf_c12': rng.uniform(*params['ctf_c12']),
        'ctf_phi12': rng.uniform(0, 2*np.pi),
        'ctf_c21':  rng.uniform(*params['ctf_c21']),
        'ctf_phi21': rng.uniform(0, 2*np.pi),
        'ctf_c23': rng.uniform(*params['ctf_c23']),
        'ctf_phi23': rng.uniform(0, 2*np.pi),
        'ctf_c32': rng.uniform(*params['ctf_c32']),
        'ctf_phi32': rng.uniform(0, 2*np.pi),
        'ctf_c34': rng.uniform(*params['ctf_c34']),
        'ctf_phi34': rng.uniform(0, 2*np.pi),
        'ctf_c45': rng.uniform(*params['ctf_c45']),
        'ctf_phi45': rng.uniform(0, 2*np.pi),
        'focal_spread': rng.uniform(*params['focal_spread']),
        ## Noise parameters
        'dose': 10**rng.uniform(*params['log_dose']),
        'readout': rng.uniform(*params['readout']),
        'blur': rng.uniform(*params['blur']),
        ## MTF parameters
        #'mtf_c0': rnd(*params['mtf_c0']),
        'mtf': params['mtf'],
        'mtf_c1': rng.uniform(*params['mtf_c1']),
        'mtf_c2': rng.uniform(*params['mtf_c2']),
        #'mtf_c3': rng.uniform(*params['mtf_c3']),
    }
    if 'mtf_c3' in params:
        tem_params['mtf_c3'] = rng.uniform(*params['mtf_c3'])

    return tem_params


# Help functions for FFT-filtering

def hexagonal_fourier_filter(image, latconst, sampling):
    """Remove spatial frequencies from a hexagonal lattice.

    image: The image to process
    latconst: The lattice constant of the lattice to remove
    sampling: The pixel size.
    """
    
    nx,ny=image.shape
    assert nx == ny
    
    # Lattice constant per sampling
    lc_samp = latconst/sampling
        
    # spread
    peakedge =20 * sampling  # Smoothness of peaks
    peaksize = 30 * sampling  # Size of peaks
    ringwidth = 10 * latconst * sampling
    
    # pixel matrix centered around 0
    x = np.arange(nx) - nx//2
    
    # filter: A ring containing the first peaks of the support
    fft_C = ringfilter(x[:,np.newaxis], x[np.newaxis,:], lc_samp, nx, ringwidth)
    # FFT transformed images
    fftimg_C = fft.fft2(image)
    fftimg_C[0,0] = 0
    shifted = fft.fftshift(fftimg_C)
    
    # rings to find the peaks in
    ring_peaks = shifted * fft_C
    signal = np.abs(ring_peaks)
    signal /= signal.max()
    
    # the peaks in the ring
    peaks = find_local_peaks(signal, min_distance=20,threshold=0.85,exclude_adjacent=True).astype(int)
    
    # the center of the array
    center = np.array([nx/2,ny/2])
    # distance from center to peak
    v = peaks[0]-center
    
    # first 6 peaks in lattice in carbon; this will have to be changed
    # based on the geometry of the sought after removed background
    peaks_1st_ring = np.array([ np.dot(rot(a), v) + center for a in np.pi/3 * np.arange(6) ])
    peaks_2nd_ring = np.array([ np.dot(rot(a), v * np.sqrt(3)) + center for a in np.pi/6 + np.pi/3 * np.arange(6)])
    peaks_3rd_ring = np.array([ np.dot(rot(a), v * 2) + center for a in np.pi/3 * np.arange(6)])
    v_peak = np.concatenate((peaks_1st_ring, peaks_2nd_ring, peaks_3rd_ring), axis=0)
    
    fftspot = spotfilters(v_peak, peaksize=peaksize, img_size=(nx,ny), alpha=peakedge)
    shift_img_no_peak = shifted * fftspot
    fft_img_no_peak = fft.ifftshift(shift_img_no_peak)

    return np.real(fft.ifft2(fft_img_no_peak))


def rot(a):
    return np.array([[np.cos(a),-np.sin(a)],[np.sin(a),np.cos(a)]])

def fermi(x, beta):
    beta_x = np.clip(beta * x, -100., 100.)
    return 1 / (np.exp(beta_x) + 1)

def ringfilter(x, y, latconst, nx, width):
    """Return a fourier filter that keeps only a ring in Fourier space.

    The ring returned correspond to a specified lattice constant, has
    a specified width, and smooth edges with a smoothness of 20% of the width.

    latconst: Lattice constant (in pixels).

    nx: Number of pixels in array returned.

    width: width of ring (in pixels).
    """
    x0 = np.arange(nx) - nx//2
    x = x0[:,np.newaxis]
    y = x0[np.newaxis,:]
    a = nx * (2 / np.sqrt(3)) / (latconst)
    a0 = a - width
    a1 = a + width
    beta = 1 / (0.2 * width )
    r = np.sqrt(x**2 + y**2)
    return fermi(r - a1, beta) - fermi(r - a0, beta)

def spotfilter(peak, peaksize, img_size, alpha):
    x=np.arange(img_size[0])-peak[0]
    y=np.arange(img_size[1])-peak[1]   
    x=x[:,np.newaxis]
    y=y[np.newaxis,:]
    a = peaksize
    beta = 1 / (0.2 * alpha )
    r = np.sqrt(x**2 + y**2)
    return fermi(a-r, beta)

def spotfilters(peaks, peaksize,img_size, alpha):
    results = spotfilter(peaks[0],peaksize,img_size, alpha)
    for p in peaks[1:]:
        results *= spotfilter(p,peaksize,img_size, alpha)
    return results


if __name__  == "__main__":
    timing = time.perf_counter()
    main()
    timing = time.perf_counter() - timing
    print(f'Total run time (wall time): {timing:.1f} s ~ {timing/60:.2f} h.')
    
