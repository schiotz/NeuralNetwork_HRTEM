#!/bin/bash
#SBATCH --job-name=adjust-images-8-layers
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon40
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=50:00:00
#SBATCH -N 1
#SBATCH -n 40

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate

export OMP_NUM_THREADS=1

python make_image_data.py ../simulation_data/test_adjusted_MoS2_supported_layers_8 parameters_exitwave.json -n 25 --train
python make_image_data.py ../simulation_data/test_adjusted_MoS2_supported_layers_8 -n 25 --test
