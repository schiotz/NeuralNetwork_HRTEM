import sys
import os
import json
import argparse
import numpy as np
# sys.path.insert(0, '../')
import multiprocessing

### import tensorflow.keras as keras
from temnn.data.mods import local_normalize
# from peakfitting.peaks import find_local_peaks, refine_peaks
from peakfitting.fit import find_local_peaks, refine_peaks

def rmse(a):
    return np.sqrt(np.mean(a*a))

def create_output_folder(datf):
    ## Create output folder ##
    if not datf.endswith("/"):
        out_dir = datf + "/"
    out_dir = datf + 'fitting_parameters/'
    
    if not os.path.exists(out_dir):
        print("Creating ", out_dir)
        os.makedirs(out_dir)
    return out_dir

def determine_sigma(arr, region=11):
    _ = arr[..., 0].squeeze() # real values
    imaginary_values = arr[..., 1].squeeze()

    peaks = find_local_peaks(image=imaginary_values,
                             min_distance=10,
                             threshold=0.30,
                             exclude_border=10,
                             exclude_adjacent=True)

    popt, messages = refine_peaks(image=imaginary_values,
                                  points=peaks)
    points, z, A, sigma = popt
    return sigma

def doit(datf, net, start, end):
    
    model_path = os.path.join(net, 'model-0')
    print('Loading model', model_path)
    
    ### model = keras.models.load_model(model_path)
    
    # Get amount of files in tem_params for loop
    #N_systems = len(os.listdir(datf + "tem_params/"))
    
    
    print(f"### Predicting and calculating sigma from {start} to {end}", flush=True)
    parameters = json.load(open(os.path.join(datf, "parameters.json")))
    # Get image size
    image_size = parameters['image_size']
    
    # # Pre-allocate memory
    # sigma_prediction = np.empty(shape=(N_systems, *image_size, 2))
    # sigma_groundTruth = np.empty(shape=(N_systems, *image_size, 2))
    # temp_list = np.empty(shape=(N_systems, 1))
    
    sigma_prediction = []
    sigma_groundTruth = []
    temp_list = []
    layers_list = []
    
    # Determine ground truth and pre
    for n in range(start, end):
        imagenumber = "{:04d}".format(n)
        try:
            imageparameters = json.load(open(os.path.join(datf, "tem_params", "parameters_000_" + imagenumber+'.json')))
        except FileNotFoundError:
            continue
        print(f"Iteration {n}", flush=True)
        tmp = np.load(os.path.join(datf, 'images_labels', 'image_label_000_'+imagenumber+'.npz'))
        image = tmp['image']
        wave = tmp['label']
        model_properties = json.load(open(os.path.join(datf, "model_properties", "model_properties_"+imagenumber+".json")))
        layers_list.append(model_properties["graphitelayers"])
        
        
        # Save temperature
        try:
            # temp_list[n] = imageparameters['Temp']
            temp_list.append(imageparameters["Temp"])
        except KeyError:
            print("`Temp` is not a file in the archive.\n Will return temp_list = [False, False, ..., False]")
            # temp_list[n] = False
            temp_list.append([None])
        
        
        
        
        sampling = imageparameters["sampling"]
        normalizedistance = parameters["normalizedistance"] / sampling
        
        ### print("Normalizing")
        ### normimage = local_normalize(image.copy(), normalizedistance, normalizedistance)
        
        
        ### print("Predicting")
        ### prediction = model.predict(normimage)
        print("Calculating `sigmas`", flush=True)
        
        sigma_groundTruth.append(determine_sigma(wave, region=11))
        ### sigma_prediction.append(determine_sigma(prediction, region=11))
        
        # sigma_groundTruth[n] = determine_sigma(wave, region=11)
        # sigma_prediction[n] = determine_sigma(prediction, region=11)
        
    return temp_list, sigma_prediction, sigma_groundTruth, layers_list

def main(datf, net):
    N_systems = len(os.listdir(datf + "tem_params/"))
    # numproc = len(os.sched_getaffinity(0))
    numproc = 10
    arguments = [(datf, net, i * N_systems // numproc, (i+1) * N_systems // numproc)
                 for i in range(numproc)]
    with multiprocessing.Pool(numproc) as pool:
        results = pool.starmap(doit, arguments)
    
    sigma_prediction = []
    sigma_groundTruth = []
    temp_list = []
    layers_list = []
    for a, b, c, d in results:
        temp_list.extend(a)
        sigma_prediction.extend(b)
        sigma_groundTruth.extend(c)
        layers_list.extend(d)
           
    temp_list = np.asarray(temp_list, dtype=object).squeeze()
    sigma_prediction = np.asarray(sigma_prediction, dtype=object).squeeze()
    sigma_groundTruth  = np.asarray(sigma_groundTruth, dtype=object).squeeze()
    layers_list = np.asarray(layers_list, dtype=object).squeeze()
    return sigma_groundTruth, sigma_prediction, temp_list, layers_list

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    positionalparams = ("datf", "network"
                        # ,"outf"
                        )
    parser.add_argument("datf", 
                        help="The path and name of the folder where the test data is placed.")
    parser.add_argument("network", 
                        help="The path and name of the folder where the trained network is placed.")
    # parser.add_argument("outf",
    #                     help="The path and name of the folder where the output is placed.")
    
    args = parser.parse_args()
    
    ## Create output folder ##
    datf = args.datf
    net = args.network
    
    ground_truth, prediction, temp, layers = main(datf, net)
    
    fpath = create_output_folder(datf)
    print(f"Output directory:\t {fpath}", flush=True)
    
    fname = fpath + "sigmas"
    np.savez(fname, gt=ground_truth, pred=prediction, temp=temp, layers=layers)