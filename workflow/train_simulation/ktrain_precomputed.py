import sys
sys.path.insert(0, '../../')
import os
import gc
import time
import platform
import shutil
import json
import argparse
import contextlib
import numpy as np
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # hide tensorflow output
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import mixed_precision
from datetime import datetime
#from temnn.analysis.analysis import find_examples, plot_example
from tqdm import tqdm
from glob import glob

#########################################################
#### Tensorflow mixed precision for memory efficiency ###
#########################################################
#policy = mixed_precision.Policy('mixed_float16')
policy = mixed_precision.Policy('float32')
mixed_precision.set_global_policy(policy)

#########################################################
###### Keras callback for monitoring epoch time #########
#########################################################
class TimingCallback(keras.callbacks.Callback):
    def __init__(self, logs={}):
        self.logs=[]
    def on_epoch_begin(self, epoch, logs={}):
        self.starttime = time.time()
    def on_epoch_end(self, epoch, logs={}):
        self.logs.append(time.time()-self.starttime)

#########################################################
###### Function for monitoring memory usage #############
#########################################################
def print_memory(txt):
    procfile = open("/proc/self/status")
    vmsize = vmpeak = vmdata = vmrss = -1
    for line in procfile:
        words = line.split()
        if words[0] == "VmSize:":
            vmsize = int(words[1])
        elif words[0] == "VmPeak:":
            vmpeak = int(words[1])
        elif words[0] == "VmData:":
            vmdata = int(words[1])
        elif words[0] == "VmRSS:":
            vmrss = int(words[1])
    print("Memory : {} MB total ({} MB peak, {} MB data, {} MB rss)".format(
        (vmsize+512)/1024,(vmpeak+512)/1024,(vmdata+512)/1024,(vmrss+512)/1024),
        file=txt)
    procfile.close()

#########################################################
############### Define Global Variables #################
#########################################################
n_e = 100               # number of training epochs
#save_epochs = True     # save the model for every epoch
save_epochs = False     # save only the latest model
reset_epochs = 5        # Reset memory after this many epochs
optimizer = 'rmsprop'   # network optimiser

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("nn",
            help="The name of the neural network being used.")
    parser.add_argument("loss",
            help="The name of the loss function used (eg. mse, crossentropy).")
    parser.add_argument("datf",
            help="The path and name of the folder where the training data is placed (remember to finish with /).")
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed (remember to finish with /).")
    parser.add_argument("outf",
            help="The name of the folder (in ../(nn)_trained_data/)where the output is placed.")
    #parser.add_argument('--visualvalidation',
    #        dest='visualvalidation', action='store_true',
    #        help='Scan through validation set every epoch and locate best, worst, and typical example.')
    parser.add_argument("--restart", default=None, type=int, help="Restart at epoch N.")
    parser.set_defaults(visualvalidation=False)
    args = parser.parse_args()
    
    #########################################################
    ################ Determine number of GPUS ###############
    ### If there are GPUS availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'CUDA_VISIBLE_DEVICES'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("Found {} GPU devices".format(n_gpu))
    else:
        n_gpu = 1
    # Print on which host this is running (useful for troubleshooting on clusters).
    print("{}: Running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))

    #########################################################
    ########## Load data and create output folders ##########
    ### There are a number of folders involved here. We will
    ### identify the input folder containing the images and
    ### labels along with their parameters, and create an
    ### output folder for the trained neural network model
    #########################################################
    # A number of output files and folders contain a timestamp as part of their name.
    nnf = args.nn + '_precomputed_trained_data' # Neural Network folder
    fn = os.path.join('../', nnf, args.outf)    # Complete output foldername
    if not os.path.exists(fn):
        print("Creating folder", fn)
        os.makedirs(fn)
    t = datetime.now().strftime("%Y%m%d-%H%M%S")
    sumf = "summaries/"+t+"/"
    nnfiles = fn+'/model-{}'
    debugf = "debug/"+t
    # Load metadata
    with open(os.path.join(args.datf, 'parameters.json')) as json_file:
        par = json.load(json_file)
    flsin = os.path.join(args.datf, 'images_labels')
    # Make folders for output
    nnf = os.path.dirname(nnfiles)
    if nnf and not os.path.exists(nnf):
        os.makedirs(nnf)
    if par['debug']:
        os.makedirs(os.path.join(fn,debugf))
    logf = open(os.path.join(nnf, t + '.log'), "wt", buffering=1)
    with open(os.path.join(args.vdatf, 'parameters.json')) as json_file:
        vpar = json.load(json_file)
    vflsin = os.path.join(args.vdatf, 'images_labels')
    
    #########################################################
    ##################### Set variables #####################
    ### We identified the input folder above and the
    ### parameters files. Now we load those parameters in
    #########################################################
    # Read number of training images
    imgs_e = par['images_per_epoch']
    imgeps = par['image_epochs']
    par['train_epochs'] = n_e
    # Read number of validation images
    vimgs_e = vpar['images_per_epoch']
    vimgeps = vpar['image_epochs']
    
    imgdim = tuple(par['image_size']) # spatial dimensions of input/output
    if par.get('multifocus', None):
        chan_in = par['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = par['num_classes'] # number of predicted class labels
    
    ### Define the loss function from input
    # & the metrics keras will use to validate along the way
    if args.loss == 'mse': # mse (preferrably for regression)
        print('Using Mean Square Error loss.')
        loss = 'mse'
        metrics=['mse']
    elif args.loss == 'crossentropy':
        if chan_out > 1:
            print('Using Categorical Cross Entropy loss.')
            loss = 'categorical_crossentropy'
        else:
            print('Using Binary Cross Entropy loss.')
            loss = 'binary_crossentropy'
        metrics=['accuracy',  
                tf.keras.metrics.Precision(),
                tf.keras.metrics.Recall()]
    
    ### Keep a copy of this script for reference ###
    shutil.copy2(__file__, nnf)
    with open(os.path.join(nnf, 'parameters.json'), "wt") as paramfile:
        json.dump(par, paramfile, sort_keys=True, indent=4)
    
    #########################################################
    ############ Set up the neural network ##################
    ### Now we can set up the given neural network with the
    ### defined optimizer, loss function, and metrics for
    ### measuring performance
    #########################################################
    # Import the specified neural network
    print('Importing', args.nn)
    if args.nn == 'Unet':
        from temnn.knet import Unet
        net = Unet
        # The model is pretty big, we probably can only train on one data
        # point at a time without running out of GPU memory.
        btch = n_gpu
        print('Training Unet architecture')
    elif args.nn == 'MSDnet':
        from temnn.knet import MSDnet
        net = MSDnet
        # The model is pretty big, we probably can only train on one data
        # point at a time without running out of GPU memory.
        btch = n_gpu
        print('Training MSDnet architecture')
    else:
        print('Please specify a correct Neural Network (Unet, or MSDnet)')
    assert(btch%n_gpu == 0)
    if n_gpu > 1: # If multi-gpu parallelisation is supported
        strategy = tf.distribute.MirroredStrategy()
        strategy_scope = strategy.scope
        print("*** Replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # Otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    with strategy_scope():
        if args.restart:
            # I will take the latest network model and restart
            latest_net = sorted(glob(fn+'/model-*'))[-1]
            print("Restarting with", latest_net)
            model = keras.models.load_model(latest_net)
        else:
            # Construct the network
            # Arbitrary input shape - this is useful so the network input
            # isn't limited to certain dimensions, in this case only a certain
            # number of channels
            x = keras.Input(shape=(None,None,chan_in))
            # Input x and out feature maps with chan_out output channels
            model = net.graph(x,
                            output_features=chan_out)#,
                            #channels=3,
                            #layers=50,
                            #kernel_size=[9,9])
            # Define the optimiser, loss function and which metrics to
            # validate
            model.compile(optimizer=optimizer,
                            loss=loss,
                            metrics=metrics)
    print("Model summary:", args.nn)
    model.summary()
    if not os.path.exists(os.path.join(fn,sumf)):
        os.makedirs(os.path.join(fn,sumf))
     
    #########################################################
    ################ Load Validation Data ###################
    ### The input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### Keras will use this to validate the network after
    ### each training epoch
    #########################################################
    print("Loading validation set", flush=True)
    vinp = np.empty((vimgs_e,imgdim[0],imgdim[1],chan_in),
                                            np.float32)
    vtar = np.empty((vimgs_e,imgdim[0],imgdim[1],chan_out),
                                            np.int8)
    for i in tqdm(range(vimgs_e)):
        d = np.load(os.path.join(vflsin,
                'image_label_{:03d}_{:04d}.npz'.format(0, i)))
        try:
            vinp[i] = d['image'].astype(np.float32)
            vtar[i] = d['label'].astype(np.int8)
        except ValueError:
            vinp[i] = d['image'][:,:imgdim[0],:imgdim[1],:].astype(np.float32)
            vtar[i] = d['label'][:,:imgdim[0],:imgdim[1],:].astype(np.int8)

    
    #########################################################
    ################### Prepare logfile #####################
    ### In the global variables at the top we defined some
    ### metrics which will be used to validate the network
    ### along the way. This is the log file where those
    ### metric values will be saved.
    #########################################################
    # Write headers to the logfile:
    line = ''
    if loss == 'mse':
        for m in ['loss',] + metrics:
            mm = 'val_' + m
            line = line + '{:<15s} {:<15s} '.format(m, mm)
    elif args.loss == 'crossentropy':
        for m in ['loss', 'accuracy', 'precision', 'recall']:
            mm = 'val_' + m
            line = line + '{:<15s} {:<15s} '.format(m, mm)
    print(line.strip(), file=logf, flush=True)
   
    #########################################################
    ##################### Training ##########################
    ### Now finally we load the training data each training
    ### epoch and train the network
    #########################################################
    # Keras callback to measure epoch time
    epoch_time_callback = TimingCallback()
    
    # Optimize the model, saving every save_epoch epoch.
    # Prepare arrays for training data
    inp = np.empty((imgs_e,imgdim[0],imgdim[1],chan_in),
                                            np.float32)
    tar = np.empty((imgs_e,imgdim[0],imgdim[1],chan_out),
                                            np.int8)

    print('Tensorflow v.{}'.format(tf.__version__))
    print('Compute dtype: %s' % policy.compute_dtype)
    print('Variable dtype: %s' % policy.variable_dtype)
    print("Starting timing")
    before = time.time()
    if args.restart is None:
        start_e = 0
    else:
        start_e = args.restart-1

    time_it = 0
    for e in range(start_e, n_e): # Training epoch loop
        # Load the training dataset (image epoch)
        img_e = e%imgeps # the image epoch
        print("Loading training [image] epoch {} [{}] / {}.".format(e+1,img_e+1,n_e),
                                                                flush=True)
        for i in tqdm(range(imgs_e)):
            d = np.load(os.path.join(flsin,
                            'image_label_{:03d}_{:04d}.npz'.format(img_e, i)))
            try:
                inp[i] = d['image'].astype(np.float32)
                tar[i] = d['label'].astype(np.int8)
            except ValueError:
                inp[i] = d['image'][:,:imgdim[0],:imgdim[1],:].astype(np.float32)
                tar[i] = d['label'][:,:imgdim[0],:imgdim[1],:].astype(np.int8)
        
        # Random permutation to shuffle them around
        rng = np.random.default_rng()
        permut = rng.permutation(imgs_e)
        inp = inp[permut]
        tar = tar[permut]
        
        # Model.fit runs the training and validates
        # The output contains the metric values
        history = model.fit(inp,tar,batch_size=btch,epochs=1,
                                validation_data=(vinp,vtar),
                                callbacks=[epoch_time_callback])
        #endtime = default_timer() - starttime
        # Log time
        if os.path.exists(nnf+"/time_{}_{}.txt".format(args.nn,args.outf)):
            mode = 'a' # append if already exists
        else:
            mode = 'w' # make a new file if not
        with open(nnf+"/time_{}_{}.txt".format(args.nn,args.outf),mode) as time_file:
            print('Epoch {}: {}'.format(e,epoch_time_callback.logs[time_it]),file=time_file)
        time_it += 1

        history = history.history  # Extract the actual history dictionary
        line = ''
        if args.loss == 'mse':
            for m in ['loss',] + metrics:
                mm = 'val_' + m
                line = line + '{:<15.8f} {:<15.8f} '.format(history[m][0],
                                                            history[mm][0])
        elif args.loss == 'crossentropy':
            for m in ['loss', 'accuracy', 'precision', 'recall']:
                mm = 'val_' + m
                line = line + '{:<15.8f} {:<15.8f} '.format(history[m][0],
                                                            history[mm][0])
        print(line.strip(), file=logf, flush=True)
        
        ### Save
        if save_epochs:
            #model.save_weights(graph_path.format(epoch))
            model.save(nnfiles.format(e)) # save a model for each epoch
        else:
            model.save(nnfiles.format(0)) # overwrite an exising model

        ### Reset memory (workaround for memory leak in TF>2.0)
        if (e+1)%reset_epochs == 0:
            print("Resetting memory.")
            keras.backend.clear_session()
            gc.collect()
            if save_epochs:
                print("Reloading from", nnfiles.format(e))
                model = keras.models.load_model(nnfiles.format(e))
            else:
                print("Reloading from", nnfiles.format(0))
                model = keras.models.load_model(nnfiles.format(0))

        ### Locate best, worst and typical examples ###
        #if args.visualvalidation:
        #    if args.loss == 'mse':
        #        idx_b, idx_w, idx_t = find_examples(model,
        #                vinp,vtar,
        #                score='mse')
        #    elif args.loss == 'crossentropy':
        #        idx_b, idx_w, idx_t = find_examples(model,
        #                vinp,vtar)
        #    fig_b = plot_example(model, vinp, vtar, idx_b)
        #    fig_b.savefig(fn+"/best_example.png")
        #    fig_w = plot_example(model, vinp, vtar, idx_w)
        #    fig_w.savefig(fn+"/worst_example.png")
        #    fig_t = plot_example(model, vinp, vtar, idx_t)
        #    fig_t.savefig(fn+"/typical_example.png")

        ### Log memory usage
        if os.path.exists(nnf+"/memory_usage_{}_{}.txt".format(args.nn,args.outf)):
            mode = 'a' # append if already exists
        else:
            mode = 'w' # make a new file if not
        with open(nnf+"/memory_usage_{}_{}.txt".format(args.nn,args.outf),mode) as text_file:
            print_memory(text_file)

    totaltime = time.time() - before
    print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    with open(nnf+"/time_{}_{}.txt".format(args.nn,args.outf),'a') as time_file:
        print("Tensorflow v.{}".format(tf.__version__),file=time_file)
        print("Total Keras epoch time: {} sec ({} hours)".format(np.sum(epoch_time_callback.logs),
            np.sum(epoch_time_callback.logs)/3600),file=time_file)
        print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600),file=time_file)
