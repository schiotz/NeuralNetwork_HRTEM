#!/bin/bash
#SBATCH --job-name=MoS2-sup-struct
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon16
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=1:00:00
#SBATCH -N 1
#SBATCH -n 1

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate

python make_supported_MoS2.py MoS2_supported_sample 100  --train
python make_supported_MoS2.py MoS2_supported_sample 100  --test

