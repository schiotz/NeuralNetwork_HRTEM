#SBATCH --mail-type=ALL
#SBATCH --mail-user=mhlla@dtu.dk  # The default value is the submitting user.
#SBATCH --partition=xeon40
#SBATCH -N 1
#SBATCH -n 40
#SBATCH --time=02-02:00:00
#SBATCH --output=MoS2-aimd-hot-%J.out

import numpy as np
from gpaw import GPAW
from ase.visualize import view
from ase.io.trajectory import Trajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution, Stationary, ZeroRotation
from ase.md.nvtberendsen import NVTBerendsen
from ase import units

# Read optimised structure
traj = Trajectory('trajectory_data/relax.traj')
atoms = traj[-1]
traj.close()

# Restart MD from trajectory
#traj = Trajectory('trajectory_data/md-2.traj')
#atoms = traj[-1]
#restart = len(traj)
#traj.close()

# Setup GPAW calculator
calc = GPAW(mode='lcao',
            basis='dzp',
            xc='PBE',
            txt='md-hot.txt',
            symmetry={'point_group': False})
atoms.calc = calc

# break symmetries, add momenta
rng = np.random.default_rng(seed=42)
MaxwellBoltzmannDistribution(atoms,
                             temperature_K=2000,
                             rng=rng)
Stationary(atoms)   # Zero linear momentum
ZeroRotation(atoms) # Zero angular momentum
#atoms.positions += rng.normal(scale=0.1,
#                              size=atoms.positions.shape)

dyn = NVTBerendsen(atoms,
                   0.5 * units.fs,
                   temperature_K=1200,
                   taut=250*units.fs,
                   trajectory='trajectory_data/md-hot.traj',
                   append_trajectory=True,
                   logfile='NVTBerendsen-hot.txt')
dyn.run(5000)
#dyn.run(5000-restart)
