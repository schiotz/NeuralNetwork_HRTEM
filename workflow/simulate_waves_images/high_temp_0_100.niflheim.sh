#!/bin/bash
#SBATCH --job-name=high-0-100-images-gpu
#SBATCH --mail-type=FAIL,END
#SBATCH --partition=sm3090_768
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=48:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --gres=gpu:RTX3090:1

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate

export OMP_NUM_THREADS=1

python make_image_data.py ../simulation_data/experimental_data_high_temp parameters_exitwave_high_temp.json -n 1 --gpu --images 0 100
