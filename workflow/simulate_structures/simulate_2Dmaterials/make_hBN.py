from maker import SampleMakerTrue2D, main
from flake import Flake
from ase import Atoms
import numpy as np

class HexagonalBoronNitrideMaker(SampleMakerTrue2D):
    numclasses = 2
    classes = {5:0, 7:1}  # Mapping from atomic numbers to classes
    save_properties = {'vacancies': 'n_vac', 'tilt': 'tilt'}

    def __init__(self, size, distort, seed):
        super().__init__(distort)
        a = 2.51
        cell = [[a, 0, 0], [-0.5*a, np.sqrt(3)/2 * a, 0], [0, 0, 2]]
        positions = [[0,0,1], [0, a/np.sqrt(3), 1]]
        prototype = Atoms(symbols='BN', positions=positions, cell=cell, pbc=[True,True,False])
        self.size = size
        self.flake = Flake(prototype, size, seed)

    def make_atoms(self):
        self.flake.make()
        self.flake.rotate()
        self.n_vac = self.flake.vacancies(0.1)
        self.flake.perturb_positions(0.1)
        self.tilt = self.flake.tilt(10)
        return self.flake.get_atoms()
    
def makehBN(first_number, last_number, dir_name, size, distort, seed):
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = HexagonalBoronNitrideMaker((size, size), distort, seed)
    maker.run(first_number, last_number, dir_name)


if __name__ == "__main__":
    main(makehBN, __file__)
    
