#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

DATASET=Au_fcc_mixed_atomic_negfoc
VDATASET=Au_fcc_mixed_atomic_negfoc-test

RESVLOCK=/tmp/lock-$DATASET-$$
python ../../reserve.py $RESVLOCK &
RESVPID=$!
sleep 10
echo "Locked GPU $CUDA_VISIBLE_DEVICES with lockfile $RESVLOCK and PID $RESVPID"

SIMULSUBFOLDER=simulation_data/$DATASET
SIMULFOLDER=/scratch/$USER/$SIMULSUBFOLDER
VSIMULSUBFOLDER=simulation_data/$VDATASET
VSIMULFOLDER=/scratch/$USER/$VSIMULSUBFOLDER
# Use this to e.g. exclude the wave subfolder when using precomputed images
RSYNCOPT='--copy-links --exclude=images --exclude=labels --exclude=debug --exclude=points --exclude=wave'


mkdir -p /scratch/$USER/$SIMULSUBFOLDER
mkdir -p /scratch/$USER/$VSIMULSUBFOLDER
rsync -av $RSYNCOPT hrid.fysik.dtu.dk:/u/raid/mhlla/$SIMULSUBFOLDER/. $SIMULFOLDER/.
#rsync -av $RSYNCOPT themis:/scratch/$USER/$SIMULSUBFOLDER/. $SIMULFOLDER/.
rsync -av $RSYNCOPT hrid.fysik.dtu.dk:/u/raid/mhlla/$VSIMULSUBFOLDER/. $VSIMULFOLDER/.
#rsync -av $RSYNCOPT themis:/scratch/$USER/$SIMULSUBFOLDER-test/. $SIMULFOLDER-test/.

# Trick the script into only using 10 cpus (be nice to other users!)
export LSB_MAX_NUM_PROCESSORS=10

# Unlock
touch $RESVLOCK
wait $RESVPID

python ktrain_precomputed.py MSDnet crossentropy $SIMULFOLDER/ $VSIMULFOLDER/ $DATASET
#python ktrain_precomputed.py MSDnet $SIMULFOLDER/ $DATASET --visualvalidation
#cd ../validate_simulation
#python learningcurve_precomputed.py ../MSDnet_precomputed_trained_data/$DATASET/

#python ktrain_precomputed.py Unet crossentropy $SIMULFOLDER/ $VSIMULFOLDER/ $DATASET-v5
#cd ../validate_simulation
#python learningcurve_precomputed.py ../Unet_precomputed_trained_data/$DATASET/

