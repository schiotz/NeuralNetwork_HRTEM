This folder duplicates code from simulate_waves, but only creates the structures.  The wavefunctions will be created together with the images in a separate part of the workflow.

This represents a change in workflow from

1. Make structures and exit waves
2. Make images
3. Train network

to

1. Make structures
2. Make exit waves and images
3. Train network.

This change is necessary to avoid storing a large amount of exit waves when frozen phonon calculations are made, only the "average" exit wave then needs to be stored.

