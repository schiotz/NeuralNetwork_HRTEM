import sys

import os
import glob

import hyperspy.api as hs
import cv2

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

import cupy as cp

from copy import copy

from matplotlib_scalebar.scalebar import ScaleBar
from mpl_toolkits.axes_grid1 import make_axes_locatable

from scipy.fft import fft2, fftshift
from scipy.ndimage.filters import maximum_filter, minimum_filter
from scipy.ndimage.measurements import center_of_mass, label
from cupyx.scipy import ndimage as cp_ndi
from cupyx.scipy.signal.signaltools import wiener
# from cupyx.scipy.fft import fft2, fftshift, ifft

from skimage.feature import blob_log, blob_doh
from skimage.color import rgb2gray

from tensorflow.keras import mixed_precision

def expand_mask(mask, radius=20):
    expand = mask.copy()
    for j in range(radius,mask.shape[0]):
        for k in range(radius,mask.shape[1]):
            window = mask[j-radius:j+radius,k-radius:k+radius]
            fraction = np.count_nonzero(window == 1.0)/(window.shape[0]*window.shape[1])
            if fraction > 0.1:
                expand[j,k] = 1.0
    return expand

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi += 2*np.pi
    return rho, phi

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y

def block_centre(img):
    c = img.shape[0]//2,img.shape[1]//2
    blocked = img.copy()
    blocked[c[0],c[1]] = 0
    return blocked

def crop_center(img,cropx,cropy):
    y,x = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy,startx:startx+cropx]

class Experimental_Data:
    def __init__(self, filepath):
        """
        Initialises class with just a path to an experimental file
        (.dm3 or .dm4)
        """
        self.filepath = filepath
        
        self.is_infered = False
        self.is_scaled = False
        self.is_window = False
        self.rect = None
        
    def load(self, filetype='dm', crop=None):
        """
        Loads the experimental data (.dm3 or .dm4) using hyperspy along with
        sampling in nm/pixel
        """
        self.data = hs.load(self.filepath)
        
        if filetype == 'dm':
            frame_metadata = self.data.original_metadata.as_dictionary()
            self.brightness_cal=frame_metadata['ImageList']['TagGroup0']['ImageData']['Calibrations']['Brightness']['Scale']
            try:
                self.exposure=frame_metadata['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
            except KeyError:
                self.exposure = None

            self.x_cal = self.data.axes_manager.signal_axes[0].scale
            self.y_cal = self.data.axes_manager.signal_axes[1].scale
        elif filetype == 'tif':
            try:
                self.x_cal = self.data.axes_manager.signal_axes[0].scale
                self.y_cal = self.data.axes_manager.signal_axes[1].scale
            except:
                print('tif file loaded. No metadata. Suggest .set_sampling(x_sampling, y_sampling) in nm.')

        try:
            if crop is not None:
                self.image = crop_center(self.data.data.astype(np.float32), crop[0], crop[1])
            else:
                self.image = self.data.data.astype(np.float32)
        except TypeError:
            image = self.data.data
            x = image.view((image.dtype[0], len(image.dtype.names)))
            x = np.squeeze(x).astype(np.float32)
            x = rgb2gray(x)
            if crop is not None:
                self.image = crop_center(x, crop[0], crop[1])
            else:
                self.image = x
        
        return self.image

    def cp_local_standardise(self, sigma_px=12.0):
        """
        Performs a local standardisation of the image.
        
        sigma_px: float - Gaussian standard deviation in pixels
        
        no return, alters image
        """
        assert self.x_cal == self.y_cal
        sigma = sigma_px/(self.x_cal*10)

        self.image = cp.asarray(self.image)
        self.image = self.image - cp_ndi.gaussian_filter(self.image,
                                                         (sigma,sigma),
                                                         mode='reflect')
        self.image = self.image / cp.sqrt(cp_ndi.gaussian_filter(self.image**2,
                                                       (sigma,sigma),
                                                       mode='reflect'))
        self.image = self.image.get()
        return self.image
    
    def fft(self, normalise=False, res=None, crop=False):
        """
        Takes the Fourier transform of the image.
        
        res: int - resolution of FT
        crop: Bool - FT of the cropped image

        returns
        Normalised Fourier Transform: ndarray - shape: res x res
        """
        if crop:
            image = self.image_crop
        else:
            if self.is_window:
                image = self.image_window.copy()
            else:
                image = self.image.copy()
        
        #img_cp = cp.asarray(image)
        #f = fftshift(fft2(img_cp)).get()
        f = fftshift(fft2(image))

        if res is not None:
            cf = f.shape[0]/2, f.shape[1]/2
            f = f[int(cf[0]-res/2):int(cf[0]+res/2), 
                  int(cf[1]-res/2):int(cf[1]+res/2)]
            
        f_abs = np.abs(f)

        if normalise:
            f_abs = (f_abs-np.min(f_abs))/(np.max(f_abs)-np.min(f_abs))
        
        return f_abs

    def compute_dose_rate(self, corner=(0,0), windowSize=(1,1)):
        """
        Function to compute dose given intensity integrated over an area
        Brightness calibration and exposure time required
        
        Return dose in units of electrons per angstrom^2 per second
        """
        if self.exposure is not None:
            image = self.data.data
            windowSize = ( int(windowSize[0]/self.x_cal), int(windowSize[0]/self.y_cal))
            roi_vac = image[corner[0]:corner[0]+windowSize[0],
                            corner[1]:corner[1]+windowSize[1]]
            area=roi_vac.shape[0]*roi_vac.shape[1]*(self.x_cal*self.y_cal)
            counts_roi=roi_vac.sum()
            intensity = counts_roi

            self.dose_rate=(intensity*self.brightness_cal)/(area*self.exposure)/100
            self.rect = patches.Rectangle((corner[0], corner[1]),
                                          windowSize[0], windowSize[1],
                                          linewidth=5, edgecolor='r', facecolor='none')
        else:
            self.dose=_rate = 0

        return self.dose_rate
    
    def compute_dose(self, corner=(0,0), windowSize=(1,1)):
        dose_rate = self.compute_dose_rate(corner, windowSize)
        self.dose = dose_rate * self.exposure
        return self.dose

    def crop_nanoparticle(self, mask=None, channel=0, smooth=20, threshold=0.5):
        """
        Takes a neural network's inference masking a nanoparticle and 
        crops the nanoparticle out of the image

        infer: numpy array - same shape as image

        returns:
        cropped image: numpy array - same shape as image
        binary mask visualising the crop: numpy array - same shape as image
        """
        # Make the inference mask binary
        if mask is None:
            inference = self.inference[0,:,:,channel]
        else:
            inference = mask
        binary = np.zeros(inference.shape)
        binary[inference >= threshold] = 1.0
        binary[inference < threshold] = 0.0
        
        # Expand the inference mask
        crop = expand_mask(binary)
        crop_cp = cp.array(crop.copy())
        crop = cp_ndi.gaussian_filter(crop_cp,                                                   
                                      (smooth, smooth),
                                      mode='reflect').get()
        
        # Crop out nanoparticle
        if self.is_window:
            image = self.image_window.copy()
        else:
            image = self.image.copy()
        self.image_crop = image*crop
        
        return self.image_crop

    def get_trimmed_imagescale(self, percent=1):
        """
        trims away a certain percent of the highest and lowest pixels
        to determine a fixed contrast scale

        perecent: int

        returns:
        trimmed image scale: tuple - min and max
        """
        flatten_pixels = np.array(self.image.flatten())
        sorted_pixels = np.sort(flatten_pixels)
        
        n = len(sorted_pixels)
        trim = int(n*percent/100)
        trimmed_pixels = sorted_pixels[trim: n-trim]

        self.scale = (min(trimmed_pixels), max(trimmed_pixels))
        self.is_scaled = True
        return self.scale 

    def window(self, start=(0,0), windowSize=(512,512)):
        self.image_window = self.image[start[0]:start[0]+windowSize[0],
                                start[1]:start[1]+windowSize[1]]
        self.is_window = True
        return self.image_window

    def infer(self, model, smooth=None, threshold=0.5):                                                                   
        """
        Takes a neural networks and runs it's                                                                             
        prediction on the image                                                                                           
        model - tensorflow neural network model
        smooth - float for sigma of Gaussian filter (default: None)
        threshold - cut-off for predictions (default: 0.5)

        returns:
        inference - numpy array                                                                                           
        """
        if self.is_window:
            image = self.image_window.copy()                                                                              
        else:
            image = self.image.copy()
        image.shape = (1,) + image.shape + (1,)

        inference = model.predict(image)
        self.is_infered = True

        if smooth is not None:
            binary_image = np.where(inference[0,:,:,0] >= threshold, 1, 0).astype(np.float32)
            binary_image_cp = cp.array(binary_image.copy())
            binary_image_blur = cp_ndi.gaussian_filter(binary_image_cp,
                                                       (smooth, smooth),
                                                       mode='reflect').get() 
            binary_image_blur.shape = (1,) + binary_image_blur.shape + (1,) 
            self.inference = binary_image_blur
        else:
            self.inference = np.where(inference >= threshold, 1, 0)
        
        return self.inference

    def plot(self, axes=False):
        if self.is_window:
            image = self.image_window.copy()
        else:
            image = self.image.copy()
        if self.is_infered:
            if self.is_scaled:
                fig, ax = plt.subplots(1,2,figsize=(24,12))
                ax[0].imshow(image.T, origin='lower', cmap='gray', vmin=self.scale[0], vmax=self.scale[1])
                ax[0].add_artist(ScaleBar(self.x_cal,
                                       units='nm',
                                       length_fraction=0.4,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                #if self.rect is not None:
                #    rect = copy(self.rect)
                #    ax[0].add_patch(rect)
                #    ax[0].set_title(f'Dose rate: {self.dose_rate:.2E} [$e^-$/$Å^2$s]',
                #                 fontdict={'fontsize': 20})
                ax[0].set_xticks([])
                ax[0].set_yticks([])
                #ax[0].tick_params(axis='both', which='both', labelsize=16)
                ax[1].imshow(self.inference[0,:,:,0].T, origin='lower', cmap='inferno')
                ax[1].set_xticks([])
                ax[1].set_yticks([])
                #ax[1].tick_params(axis='both', which='both', labelsize=16)
            else:
                fig, ax = plt.subplots(1,2,figsize=(24,12))
                ax[0].imshow(image.T, origin='lower', cmap='gray')
                ax[0].add_artist(ScaleBar(self.x_cal,
                                       units='nm',
                                       length_fraction=0.4,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                #if self.rect is not None:
                #    rect = copy(self.rect)
                #    ax[0].add_patch(rect)
                #    ax[0].set_title(f'Dose rate: {self.dose_rate:.2E} [$e^-$/$Å^2$s]',
                #                 fontdict={'fontsize': 20})
                ax[0].set_xticks([])
                ax[0].set_yticks([])
                #ax[0].tick_params(axis='both', which='both', labelsize=16)
                ax[1].imshow(self.inference[0,:,:,0].T, origin='lower', cmap='inferno')
                ax[1].set_xticks([])
                ax[1].set_yticks([])
                #ax[1].tick_params(axis='both', which='both', labelsize=16)
        else:
            if self.is_scaled:
                fig, ax = plt.subplots(1,1,figsize=(12,12))
                ax.imshow(image.T, origin='lower', cmap='gray', vmin=self.scale[0], vmax=self.scale[1])
                ax.add_artist(ScaleBar(self.x_cal,
                                       units='nm',
                                       length_fraction=0.4,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                if self.rect is not None:
                    ax.add_patch(self.rect)
                    ax.set_title(f'Dose: {self.dose:.0f} [$e^-$/$Å^2$]',
                                 fontdict={'fontsize': 25})
                ax.set_xticks([])
                ax.set_yticks([])
                #ax.tick_params(axis='both', which='both', labelsize=16)
            else:
                fig, ax = plt.subplots(1,1,figsize=(12,12))
                ax.imshow(image.T, origin='lower', cmap='gray')
                ax.add_artist(ScaleBar(self.x_cal,
                                       units='nm',
                                       length_fraction=0.4,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                if self.rect is not None:
                    ax.add_patch(self.rect)
                    ax.set_title(f'Dose: {self.dose:.0f} [$e^-$/$Å^2$]',
                                 fontdict={'fontsize': 25})
                ax.set_xticks([])
                ax.set_yticks([])
                #ax.tick_params(axis='both', which='both', labelsize=16)
        plt.tight_layout()
        plt.gcf().set_dpi(300)
        if axes:
            return fig,ax
        else:
            return fig
    def get_sampling(self):
        return (self.x_cal, self.y_cal)
    
    def get_image(self):
        return self.image
    
    def get_inference(self):
        return self.inference
                  
    def set_sampling(self, x, y):
        self.x_cal = x
        self.y_cal = y

    def set_image(self, image):
        self.image = image

    def set_inference(self, inference):
        if not self.is_infered:
            self.is_infered = True
        if len(inference.shape) == 2:
            inference.shape = (1,) + inference.shape + (1,)
        self.inference = inference

class Experimental_DataCube:
    def __init__(self, files):
        self.DataCube = []
        for f in files:
            self.DataCube.append(Experimental_Data(f))
        self.is_scaled = False
        self.is_infered = False
        self.is_window = False

    def load(self, filetype='dm'):
        self.ImageCube = []
        for dcube in self.DataCube:
            self.ImageCube.append(dcube.load(filetype=filetype))
       
        self.ImageCube = np.moveaxis(np.asarray(self.ImageCube).astype(np.float32),[0,1,2],[2,0,1])
        return self.ImageCube

    def get_trimmed_imagescale(self, percent=1):
        self.ScaleCube = []
        for dcube in self.DataCube:
            self.ScaleCube.append(dcube.get_trimmed_imagescale())
        self.is_scaled = True
        return self.ScaleCube
    
    def cp_local_standardise(self, sigma_px=12.0):
        """
        Performs a local standardisation of the image.
        
        sigma_px: float - Gaussian standard deviation in pixels
        
        no return, alters image
        """
        self.ImageCube = cp.asarray(self.ImageCube)
        channels = self.ImageCube.shape[-1]
        B=cp.zeros_like(self.ImageCube[:,:,0])
        S=cp.zeros_like(self.ImageCube[:,:,0])
        for i in range(channels):
            sigma_x = sigma_px/(self.DataCube[i].x_cal*10)
            sigma_y = sigma_px/(self.DataCube[i].y_cal*10)
            B += cp_ndi.gaussian_filter(self.ImageCube[:,:,i],
                                        (sigma_x,sigma_y),
                                        mode='reflect')
        for i in range(channels):
            self.ImageCube[:,:,i] = self.ImageCube[:,:,i] - B/channels
        for i in range(channels):
            sigma_x = sigma_px/(self.DataCube[i].x_cal*10)
            sigma_y = sigma_px/(self.DataCube[i].y_cal*10)
            S += cp.sqrt(cp_ndi.gaussian_filter(self.ImageCube[:,:,i]**2,
                                                (sigma_x,sigma_y),
                                                mode='reflect'))
        for i in range(channels):
            self.ImageCube[:,:,i] = self.ImageCube[:,:,i] / (S/channels)
        self.ImageCube = self.ImageCube.get()
        for i in range(channels):
            self.DataCube[i].set_image(self.ImageCube[:,:,i])
        return self.ImageCube

    def window(self, start=(0,0), windowSize=(512,512)):
        self.ImageCube_window = []
        for i,dcube in enumerate(self.DataCube):
            self.ImageCube_window.append(dcube.window(start=start, windowSize=windowSize))
        self.ImageCube_window = np.moveaxis(np.asarray(self.ImageCube_window).astype(np.float32),[0,1,2],[2,0,1])
        self.is_window = True
        return self.ImageCube_window

    def infer(self, model):
        if self.is_window:
            ImageCube = self.ImageCube_window.copy()
        else:
            ImageCube = self.ImageCube.copy()
        ImageCube.shape = (1,) + ImageCube.shape

        self.inference = model.predict(ImageCube)
        self.is_infered = True
        return self.inference

    def plot(self):
        if self.is_window:
            ImageCube = self.ImageCube_window.copy()
        else:
            ImageCube = self.ImageCube.copy()
        if self.is_scaled:
            channels = ImageCube.shape[-1]
            width = 36.0
            
            nplots = channels
            fig = plt.figure(figsize=(width,width/nplots))
            for i in range(channels):
                ax = fig.add_subplot(1, nplots, i+1)
            
                pos = ax.imshow(ImageCube[:,:,i].T,
                        origin='lower', cmap='gray',
                        vmin=self.ScaleCube[i][0],vmax=self.ScaleCube[i][1])
                ax.add_artist(ScaleBar(self.DataCube[i].x_cal,
                                       units='nm',
                                       length_fraction=0.3,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size="2%", pad=0.05)
                cbar = fig.colorbar(pos, cax=cax)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
        else:
            channels = ImageCube.shape[-1]
            width = 36.0
            
            nplots = channels
            fig = plt.figure(figsize=(width,width/nplots))
            for i in range(channels):
                ax = fig.add_subplot(1, nplots, i+1)
            
                pos = ax.imshow(ImageCube[:,:,i].T,
                        origin='lower', cmap='gray')
                ax.add_artist(ScaleBar(self.DataCube[i].x_cal,
                                       units='nm',
                                       length_fraction=0.3,
                                       color='k',
                                       frameon=True,
                                       location='lower right',
                                       font_properties={'size': 20}))
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size="2%", pad=0.05)
                cbar = fig.colorbar(pos, cax=cax)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
        plt.tight_layout()
        return fig
    
    def set_inference(self, inference):
        if len(inference.shape) == 2:
            inference.shape = (1,) + inference.shape + (1,)
        else:
            inference.shape = (1,) + inference.shape
        self.is_infered = True
        self.inference = inference

class Fourier_Transform:
    def __init__(self, fft):
        """
        Initialiases with an FT as a numpy array and setting
        peaks and symmetric peaks flags to False. These are
        toggled if peaks and symmetric peaks are found in the 
        FT.
        """
        self.fft = fft

        self.is_peaks = False
        self.is_sym_peaks = False

    def find_peaks(self,threshold=0.1):
        """
        Applies scikit image's LOG blob detection to locate peaks in the FFT.
        """
        fft = block_centre(self.fft)
        self.peaks = blob_log(fft, overlap=0, max_sigma=10, threshold=np.max(fft)*threshold)

        if len(self.peaks) > 0:
            self.is_peaks = True
        return self.peaks
    
    def symmetric_peaks(self, buffer=0.01):
        """
        Locates symmetrics peaks and pairs them.

        Returns:
        List of pairs of peaks (i.e. (x1,y1,x2,y2))
        """
        if self.is_peaks:
            centroid = (sum(self.peaks[:,0]) / len(self.peaks),
                        sum(self.peaks[:,1]) / len(self.peaks))
            cpeaks = self.peaks[:,:-1] - centroid
            
            # Convert all peaks from cartesian to polar coordinates
            polarpeaks = np.zeros(cpeaks.shape)
            for i, peak in enumerate(cpeaks):
                polarpeaks[i,0], polarpeaks[i,1] = cart2pol(peak[0],peak[1])
            # Sort the peaks by increasing angle
            polarpeaks = np.array(sorted(polarpeaks,key=lambda x: x[1]))

            self.sym_peaks = [] # this will store symmetric peaks
            for i, polpeak1 in enumerate(polarpeaks):
                # For every peak, loop through all other peaks to find a symmetric peak
                for j, polpeak2 in enumerate(polarpeaks[i+1:]):
                    # This if statement matches the length and symmetry (angle+Pi) of every point 
                    # to find a symmetric pair
                    if (
                        polpeak1[0] >= polpeak2[0]-buffer and 
                        polpeak1[0] <= polpeak2[0]+buffer and 
                        polpeak1[1]+np.pi >= polpeak2[1]-buffer and 
                        polpeak1[1]+np.pi <= polpeak2[1]+buffer
                    ):
                        cartp1_x, cartp1_y = pol2cart(polpeak1[0],polpeak1[1])
                        cartp2_x, cartp2_y = pol2cart(polpeak2[0],polpeak2[1])
                        
                        self.sym_peaks.append( (cartp1_x+centroid[0], cartp1_y+centroid[1],
                                                cartp2_x+centroid[0], cartp2_y+centroid[1]) )
            
            if len(self.sym_peaks) > 0:
                self.is_sym_peaks = True
            elif len(self.sym_peaks) == 0:
                self.sym_peaks = 0
        else:
            print('No symmetric peaks available!')
            self.sym_peaks = 0
     
        return self.sym_peaks

    def summed_peaks(self):
        """
        Sums up intensity values at symmetric peak positions.
        
        Intensity is Peak intensity/average intensity with

        Returns:
        Sum of peak intensities - float
        """
        if self.is_peaks and self.is_sym_peaks:
            if len(self.sym_peaks)<4:
                radius = np.max(self.peaks[:,-1])
                self.sum_peaks = 0
                for p in self.sym_peaks:
                    self.sum_peaks += np.sum(self.fft[int(p[0]-radius):int(p[0]+radius),
                                                      int(p[1]-radius):int(p[1]+radius)])
                return self.sum_peaks / np.mean(self.fft)
            else:
                return 0
        else:
            print('No symmetric peaks available!')
            return 0
    
    def brightness_ratio(self):
        """
        Obtains the intensity value of each symmetric peak pair
        and returns the ratio between the minimum and maximum 
        brightness

        Rturns:
        Brightness ratio (min/max) - float)
        """
        if self.is_peaks and self.is_sym_peaks:
            radius = np.max(self.peaks[:,-1])
            if len(self.sym_peaks)>1 and len(self.sym_peaks)<4:
                self.br = []
                for p in self.sym_peaks:
                    br1 = self.fft[int(p[0]),int(p[1])] / \
                                        np.mean(self.fft[int(p[0]-radius):int(p[0]+radius),
                                                         int(p[1]-radius):int(p[1]+radius)])
                    br2 = self.fft[int(p[2]),int(p[3])] / \
                                        np.mean(self.fft[int(p[2]-radius):int(p[2]+radius),
                                                         int(p[3]-radius):int(p[3]+radius)])
                    self.br.append( br1 + br2 )

                return min(self.br)/max(self.br)
            else:
                return 0.1
        else:
            print('No symmetric peaks available!')
            return 0.01

    def plot(self):
        """
        Plots the Fourier Transform, along with identified
        peaks and symmetric peaks if available
        """
        if self.is_peaks:
            fig, ax = plt.subplots(1,1,figsize=(12,12))
            pos = ax.imshow(self.fft.T,origin='lower', cmap='gray')
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='2%', pad=0.05)
            cbar = fig.colorbar(pos, cax=cax)
            cbar.ax.tick_params(labelsize=16)
            for blob in self.peaks:
                x, y, r = blob
                c = plt.Circle((x, y), r, color='r', linewidth=2, fill=False)
                ax.add_patch(c)
            if self.is_sym_peaks:
                for peak in self.sym_peaks:
                    ax.scatter((peak[0],peak[2]),(peak[1],peak[3]),
                               s=500, marker='x')
            ax.set_xticks([])
            ax.set_yticks([])
            #ax.tick_params(axis='both', which='both', labelsize=16)
        else:
            fig, ax = plt.subplots(1,1,figsize=(12,12))
            pos = ax.imshow(self.fft.T,origin='lower', cmap='gray')
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='2%', pad=0.05)
            cbar = fig.colorbar(pos, cax=cax)
            cbar.ax.tick_params(labelsize=16)
            ax.set_xticks([])
            ax.set_yticks([])
            #ax.tick_params(axis='both', which='both', labelsize=16)
        plt.tight_layout()
        return fig

