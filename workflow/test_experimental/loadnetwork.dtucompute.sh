#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

NETWORK=MSDnet_precomputed_trained_data
DATASET=Au_fcc_mixed

SIMULSUBFOLDER=$NETWORK/$DATASET
SIMULFOLDER=/scratch/$USER/$SIMULSUBFOLDER

mkdir -p /scratch/$USER/$SIMULSUBFOLDER
rsync -av hrid.fysik.dtu.dk:/u/raid/mhlla/$SIMULSUBFOLDER/model-* hrid.fysik.dtu.dk:/u/raid/mhlla/$SIMULSUBFOLDER/parameters.json $SIMULFOLDER/.

# Trick the script into only using 10 cpus (be nice to other users!)
export LSB_MAX_NUM_PROCESSORS=10
