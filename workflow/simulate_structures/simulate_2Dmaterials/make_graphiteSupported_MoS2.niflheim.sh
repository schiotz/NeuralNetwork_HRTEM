#!/bin/bash
#SBATCH --job-name=MoS2-sup-struct
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon16
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=1:00:00
#SBATCH -N 1
#SBATCH -n 1
#
# Necessary modules
source $HOME/development/NeuralNetwork_HRTEM/venv-2023a/bin/activate

echo VIRTUAL ENVIRONMENT: $VIRTUAL_ENV

#
python make_graphiteSupported_MoS2.py MoS2_graphite 100 --graphite 6 --train 
python make_graphiteSupported_MoS2.py MoS2_graphite 500 --graphite 6 --test
