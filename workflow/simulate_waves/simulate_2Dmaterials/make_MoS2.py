from maker import SampleMakerDistinctColumns, main
from flake import Flake
from ase import Atoms
from ase.build import mx2
import numpy as np

class MoS2Maker(SampleMakerDistinctColumns):
    numclasses = 4
    classes = {(42,): 0, (16,16): 1, (16,):2}
    defaultclass = 3     # This class is for any other combination of atomic numbers

    def __init__(self, size, beamenergy, debyewaller, distort, seed):
        super().__init__(beamenergy, debyewaller, distort)
        prototype = mx2(formula='MoS2')
        self.size = size
        self.flake = Flake(prototype, size, seed)

    def make_atoms(self):
        self.flake.make()
        self.flake.rotate()
        self.n_vac = self.flake.vacancies(0.2)
        self.n_holes = self.flake.holes(0)
        if self.distort:
            # Some samples should have larger displacements of atoms than others
            # Pull typical displacement from lognormal distribution with mean of 0.1 Å
            displ = np.clip(self.flake.rng.lognormal(np.log(self.distort), np.exp(-1)), 0.0, 0.5)
            self.flake.perturb_positions(displ)
        else:
            self.flake.perturb_positions(0.01)
        self.tilt = self.flake.tilt(1)  # Only a small tilt to avoid breaking colums of S2

        return self.flake.get_atoms()
            
def makeMoS2(first_number, last_number, dir_name, npoints, sampling, beamenergy, debyewaller, distort, seed):
    size = npoints * sampling
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("Number of point in wave function: {} x {}".format(npoints, npoints))
    print("Sampling: {} Å/pixel".format(sampling))
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    if debyewaller:
        print("Debye-Waller factor <u^2>: {:.5f} Å^2".format(debyewaller))
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = MoS2Maker((size, size), beamenergy, debyewaller, distort, seed)
    maker.run(first_number, last_number, dir_name, npoints, sampling)


if __name__ == "__main__":
    main(makeMoS2, __file__)
    
