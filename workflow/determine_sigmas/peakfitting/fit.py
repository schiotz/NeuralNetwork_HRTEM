import numpy as np
from scipy.ndimage.filters import maximum_filter, minimum_filter
from scipy.ndimage.measurements import center_of_mass, label
# from skimage.filters import gaussian
from scipy.optimize import least_squares
from tqdm import tqdm



def find_local_peaks(image, min_distance, threshold=0, local_threshold=0, 
                    exclude_border=0, exclude_adjacent=False):
    
    """Return peaks in an image as a Points object.
    
    Peaks are the local maxima in a region of `2 * min_distance + 1`
    (i.e. peaks are separated by at least `min_distance`).
    
    A maximum filter is used for finding local maxima. This operation dilates 
    the original image. After comparison of the dilated and original image, 
    this function returns the coordinates or a mask of the peaks where the 
    dilated image equals the original image.
    
    Parameters
    ----------
    image : ndarray
        Input image.
    min_distance : int
        Minimum number of pixels separating peaks in a region of `2 *
        min_distance + 1` (i.e. peaks are separated by at least
        `min_distance`).
        To find the maximum number of peaks, use `min_distance=1`.
    threshold : float, optional
        Minimum relative intensity of peaks. By default, the threshold 
        is zero.
    local_threshold : float, optional
        Minimum local relative intensity of peaks. A minimum filter is used 
        for finding the baseline for comparing the local intensity.
        By default, the local threshold is zero.
    exclude_border : int, optional
        If nonzero, `exclude_border` excludes peaks from
        within `exclude_border`-pixels of the border of the image.
    exclude_adjacent : bool, optional
        In case of flat peaks (i.e. multiple adjacent pixels have 
        identical intensities), if true, only the mean pixel position 
        will be returned.
    
    """
    
    image = image.astype(np.float32)
    
    threshold = image.min() + threshold*(image.max()-image.min())
    
    max_filt = maximum_filter(image, min_distance)
    
    is_peak = (image == max_filt)
    
    is_peak[image < threshold] = False
    
    if local_threshold > 0:
        local_threshold = image.min() + local_threshold * (image.max()-image.min())
        min_filt = minimum_filter(image, min_distance)
        is_peak[(max_filt - min_filt) < local_threshold] = False
    
    if exclude_border:
        is_peak[0:exclude_border+1,:] = False
        is_peak[:,0:exclude_border+1] = False
        is_peak[-1:-exclude_border-1:-1,:] = False
        is_peak[:,-1:-exclude_border-1:-1] = False
        
    if exclude_adjacent:
        labels = label(is_peak)
        peaks = center_of_mass(np.ones_like(labels[0]), labels[0], range(1,labels[1]+1))
        peaks = np.array(peaks)
    else:
        peaks = np.array(np.where(is_peak)).T

    return peaks


def fit_gaussian(sub_image):
    '''
    Get refined peak position in sub_image, using a gaussian-fit.
    
    Parameters
    ----------
        ``sub_image``: The input image; a sub_image centered around a peak.T
        
    Returns
    -------
        ``popt``: The parameters optimized using least squares.
        ``message``: A message describing the concergence of the leas_squares() method.    
    '''
    
    x, y = np.indices(sub_image.shape)
    
    x = x.ravel()
    y = y.ravel()
    z = sub_image.ravel()
    
    def residuals(p):
        # x0, y0, z0, A, a = p
        
        return gauss(x, y, *p) - z
    
    
    initial = np.array([6, 6, z.min(), z.max() - z.min(), 3])
    
    size = np.sqrt(len(z))
    
    lower_z = -0.7
    upper_z = 0.7
    bounds = [(-size, -size, lower_z,      0,      0),
              ( size,  size, upper_z, np.inf, np.inf)]
    
    ls = least_squares(residuals, initial, bounds=bounds)
    # print(ls.cost)
    return ls.x, ls.message

def gauss(x, y, *params):
    '''
    The gaussian of which to fit the parameters.
    
    Parameters
    ----------
        ``x``: A ndarray of shape (N,1)
        ``y``: A ndarray of shape (N,1)
        ``*Params``: A tuple of ``5`` parameters corresponding to the values of: ``x0``, ``y0``, ``z0``, ``Amplitude``, ``a = 1/(2*sigma**2)``
    
    Returns
    -------
        ``out``: A ndarray of shape (N,1) of the corresponding fit values.
    '''
    x0, y0, z0, A, a = params
    
    # exponent = -1/2 * ( (x - x0)**2 + (y - y0)**2)/a**2
    exponent = -a * ( (x - x0)**2 + (y - y0)**2 )
    return z0 + A*np.exp(exponent)

def get_peaks(image):
    r'''
    Get peaks from method find_local_peaks()
    
    Parameters
    ----------
        ``image``: An array of size (M, N)
    
    Returns
    -------
        ``peaks``: An array of size (K, 2) of the (x,y) positions of the peaks as integers.
    
    '''
    peaks = find_local_peaks(image, 
                            threshold=0.3, 
                            min_distance=10, 
                            exclude_border=0,
                            exclude_adjacent=True)
    
    return peaks.astype(int).squeeze()

def get_image(image, peak, OFFSET):
    '''
    Get a ``sub_image`` of ``image`` around the ``peak``.
    
    Parameters
    ----------
        ``image``: The image of the whole system.
        ``peak``: The position of the peak to be analysed. A list of utple of (x,y)-coordinates.
        
    Returns
    -------
        ``sub_image``: An image centered around the ``peak``.
    '''
    
    peak = peak.astype(int)
    im = image[peak[0]-OFFSET:peak[0]+OFFSET, peak[1]-OFFSET:peak[1]+OFFSET]
    return im

def get_parameters(sub_image):
    
    '''
    Determine parameters of a fit for a specific sub_image
    '''
    
    popt, message = fit_gaussian(sub_image)
    x0, y0, z0, A, a = popt
    
    sigma = np.sqrt(1/(2*a))
    
    return (x0, y0, z0, A, sigma), message

def refine_peaks(image, points):

    ref_p = []
    z = []
    A = []
    sigmas = []
    messages = []
    OFFSET = 6
    for p in points:
        if np.any(p < OFFSET) or np.any(p > image.shape[0] - OFFSET):
            continue
        sub_image = get_image(image, p, OFFSET=OFFSET)
        sub_peak = get_peaks(sub_image)
        popt, message = get_parameters(sub_image)

        ref_p.append([popt[0] - OFFSET + p[0], popt[1] - OFFSET + p[1]])
        z.append(popt[2])
        A.append(popt[3])
        sigmas.append(popt[4])
        messages.append(message)
    
    ref_p = np.asarray(ref_p)
    z = np.asarray(z)
    A = np.asarray(A)
    sigmas = np.asarray(sigmas)
    messages = np.asarray(messages)
    return (ref_p, z, A, sigmas), messages