import make_graphiteSupported_MoS2
from maker import main
from scipy.cluster.hierarchy import fcluster, linkage
import numpy as np

class GraphiteSupportedMoS2MakerVacancyClass(make_graphiteSupported_MoS2.GraphiteSupportedMoS2Maker):
    """Makes samples of MoS2 on a graphene support.

    The graphene is ignored in the sites and classes output.  Columns
    containing one Mo are class 0, those containing two S are class 1, and
    one S are class 2 ("S vacancy").  No columns should contain any other
    combination, but if present they will be class 3 (background) if 
    allow_unknown is True, or result in an error otherwise.
    """
    numclasses = 3
    species = {(42,): 0, (16,16): 1, (16,):2}
    allow_unknown = False
    unknown_class = 3

    def make_data(self, _unused):
        allatoms = self.make_atoms()
        atoms = allatoms[allatoms.numbers != 6]   # Discard C atoms
        
        # Create positions of the columns.
        positions = atoms.get_positions()[:,:2]
        z = atoms.get_atomic_numbers()
        clusters = fcluster(linkage(positions), self.columndistance, criterion='distance')
        unique = np.unique(clusters)
        # Now cluster is the column number each atom belongs to, and unique is a list of all columns
        sites = np.zeros((len(unique), 2))    # Positions of the colums
        classes = -np.ones(len(unique), int)  # classes of the columns.  Initialize to -1
        for i, u in enumerate(unique):
            sites[i] = np.mean(positions[clusters==u], axis=0)
            atnos = tuple(sorted(z[clusters==u]))
            try:
                c = self.species[atnos]
            except KeyError:
                if self.allow_unknown:
                    c = self.unknown_class
                else:
                    raise RuntimeError(f'Found unexpected atomic column with atoms {str(atnos)}.')
            classes[i] = c
        return allatoms, positions, sites, classes

def makeMoS2_Vac(first_number, last_number, dir_name, size, distort, seed, graphitelayers):
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = GraphiteSupportedMoS2MakerVacancyClass((size, size), distort, seed, graphitelayers)
    maker.run(first_number, last_number, dir_name)

if __name__ == "__main__":
    main(makeMoS2_Vac, [__file__, make_graphiteSupported_MoS2.__file__], graphite=True)
