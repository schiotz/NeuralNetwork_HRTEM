#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

NETWORK=../MSDnet_precomputed_trained_data/Au_fcc_10_new_blob_new
TASK=objloc_spotlight

#################################################################
#HEAD=../experimental_data/WilliamBang/20201126_ETEM_MEMS6
#HEADOUT=$NETWORK/$(basename $HEAD)
##for ROI in $HEAD/*/
#for ROI in "$HEAD/ROI1/" ###"$HEAD/ROI2/" "$HEAD/ROI3/"
#do
#  for i in $ROI*/
#  do
#    for j in $i*/
#    do
#        EXPDATA=$j
#        OUT=$HEADOUT/$(basename $ROI)/$(basename $i)/$(basename $EXPDATA)
#        python analyze_movie.py $NETWORK $EXPDATA $OUT
#        python $TASK/postprocess_expt_movie.py $EXPDATA $OUT --sampling=0.0735
#    done
#  done
#done
#################################################################
#HEAD=../experimental_data/WilliamBang/20201201_ETEM_MEMS6
#HEADOUT=$NETWORK/$(basename $HEAD)
#for ROI in $HEAD/*/
##for ROI in "$HEAD/ROI3/" "$HEAD/ROI4/"
#do
#  for EXPDATA in $ROI*/
#  do
#    OUT=$HEADOUT/$(basename $ROI)/$(basename $EXPDATA)
#    python analyze_movie.py $NETWORK $EXPDATA $OUT
#    python $TASK/postprocess_expt_movie.py $EXPDATA $OUT --sampling=0.0735
#  done
#done
#################################################################
HEAD=../experimental_data/WilliamBang/20210316_ETEM_MEMS8
HEADOUT=$NETWORK/$(basename $HEAD)
#for EXAMPLE in $HEAD/
#for EXAMPLE in "$HEAD/AuCeO2_defoc9/"
#do
for EXPDATA in $HEAD/*/
do
  OUT=$HEADOUT/$(basename $EXPDATA)
  python analyze_movie.py $NETWORK $EXPDATA $OUT --crop 500 2000 2500 4000
  python $TASK/postprocess_expt_movie.py $EXPDATA $OUT --crop 500 2000 2500 4000
done
#done
