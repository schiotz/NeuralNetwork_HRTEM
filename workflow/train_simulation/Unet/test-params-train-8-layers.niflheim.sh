#!/bin/bash
#SBATCH --job-name=test-params-train-8-layers
#SBATCH --mail-type=FAIL,END
#SBATCH --partition=sm3090_768
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=72:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --gres=gpu:RTX3090:1

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate

export OMP_NUM_THREADS=1

python train_imageepochs.py 200 ../../simulation_data/test_params_MoS2_layers_8 ../../simulation_data/test_params_MoS2_layers_8-test ../../trained_networks/test_params_MoS2_layers_8

python validate_histogram.py ../../simulation_data/test_params_MoS2_layers_8-test ../../trained_networks/test_params_MoS2_layers_8

