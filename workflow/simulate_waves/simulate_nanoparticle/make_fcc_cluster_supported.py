## for importing modules from parent directories
import sys
sys.path.insert(0, '../../../../NeuralNetwork_HRTEM')
## import modules
import numpy as np
import matplotlib.pyplot as plt
import ase.io
import ase.build
import os
import time
import shutil
import argparse
import subprocess
from ase import Atoms
from ase.build import bulk, surface
from ase.cluster.cubic import FaceCenteredCubic
from scipy.cluster.hierarchy import fcluster, linkage
from temnn.reproducible import make_reproducible
from abtem.potentials import Potential
from abtem.waves import PlaneWave
import multiprocessing

#########################################################
###### Function for monitoring memory usage #############
#########################################################
def print_memory(txt):
    procfile = open("/proc/self/status")
    vmsize = vmpeak = vmdata = vmrss = -1
    for line in procfile:
        words = line.split()
        if words[0] == "VmSize:":
            vmsize = int(words[1])
        elif words[0] == "VmPeak:":
            vmpeak = int(words[1])
        elif words[0] == "VmData:":
            vmdata = int(words[1])
        elif words[0] == "VmRSS:":
            vmrss = int(words[1])
    print("Memory : {} MB total ({} MB peak, {} MB data, {} MB rss)".format(
        (vmsize+512)/1024,(vmpeak+512)/1024,(vmdata+512)/1024,(vmrss+512)/1024),
        file=txt)
    procfile.close()

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, dir_name, N, sampling, element, tilt, seed):
    """Create nanoparticles, their exit waves and the ground truth for training.

    This function is creating a sequence of nanoparticles.  It is
    intended to be called in multiple processes in parallel, so the
    start and end number of the sequence is specified.

    first_number: int
        Sequence number of the first nanoparticle to be generated.
    last_number: int
        One more than the last nanoparticle to be generated.
    dir_name: string
        Name of the folder where the output is placed.
    N: int
        Size of the wavefunction array (will be N*N 128-bit complex numbers).
    sampling: float
        Resolution of the wavefunctions, in Angstrom per pixel.
    element: str
        Chemical element of the nanoparticle
    tilt: float
        Maximal off-axis tilt of nanoparticle, in degrees.  1.0 to 2.0 are good values.
    seed: np.random.SeedSequence or similar suitable seed
        Seed for the random number generator.
    """
    # Parameters
    L=sampling*N   # Cell size in Angstrom

    # Initialize random generator
    rng = np.random.default_rng(seed)
    
    # Create the models
    # The first mention of miller indices inclues the family of planes
    # The second mention allows for truncation of that specific layer
    for i in range(first_number, last_number):

        # randomly pick a z-thickness of the cell
        percent = rng.uniform(0.75,0.99)
        thick_choice = rng.choice([L, percent*L])

        ## Build Ceria slab
        #ceria = bulk('CeO2', crystalstructure='fluorite',a=3.866,b=3.866,c=3.866,alpha=60)
        ceria = bulk('CeO2', crystalstructure='fluorite', a=5.4113412, b=5.4113412, c=5.4113412 ,alpha=90)
        slab = surface(ceria, indices=(1,1,1), layers = 20, vacuum = 0) 
        rep = rng.integers(low=20, high=25, endpoint=True)
        slab *= (rep,rep,1)
        slab.set_cell((L,L,thick_choice))
        slab.center()
        slab.rotate((0, 0, 1), (-1, 0, 0), center='COU') # align interface
 
        ## Build Au nanoparticle
        # Generate a number layers for a family of surfaces
        l100 = rng.integers(low=7, high=15, endpoint=True)
        l110 = rng.integers(low=7, high=15, endpoint=True)
        l111 = rng.integers(low=7, high=15, endpoint=True)
        # Truncate specific layers (111)
        t111 = rng.integers(low=-2, high=2)
        surfaces = [(1, 0, 0), (1, 1, 0), (1, 1, 1), (1, 1, 1)]
        layers = [l100, l110, l111, t111]
        # Construct atoms (Uses ASE.data for lattice constant)
        atoms = FaceCenteredCubic(element, surfaces, layers)
        atoms.rotate((1, 1, 1), (1, 0, 0))     # align interface
        atoms.rotate((0, -1, 1), (0, 0, -1))   # align zone axis
        atoms.cell = slab.cell
        atoms.center()

        ## Stitch nanoparticles and slab together
        interface_space = 1.0 # space between nanoparticle and substrate [Å]
        x_atoms = np.max(atoms.positions[:,0])
        x_slab = np.min(slab.positions[:,0])
        atoms.translate([-(x_atoms-L/2+interface_space),0,0])
        slab.translate([(-x_slab+L/2+interface_space),0,0])

        # Two possible alignments
        flip_choice = rng.choice([0,180])
        atoms.rotate(v='x',a=flip_choice, center='COU')
        # shift atoms 
        atom_pos = atoms.positions
        max_x = max(atom_pos[:,0])
        x_shift = abs(L/2-max_x)
        atoms.translate([-x_shift, rng.integers(low=-10, high=10, endpoint=True), 0])
        
        ## Adsorb nanoparticle to slab
        system = slab.copy()
        system.extend(atoms)

        limit = 2.0   # Minimal amount of space along edges.
        # Apply a random tilt        
        omega=rng.random()*360
        alpha=rng.random()*tilt
        atoms.rotate(v='z', a=omega, center='COU')
        atoms.rotate(v='y', a=alpha, center='COU')
        atoms.rotate(v='z', a=-omega, center='COU')
        slab.rotate(v='z', a=omega, center='COU')
        slab.rotate(v='y', a=alpha, center='COU')
        slab.rotate(v='z', a=-omega, center='COU')
        system.rotate(v='z', a=omega, center='COU')
        system.rotate(v='y', a=alpha, center='COU')
        system.rotate(v='z', a=-omega, center='COU')
        
        # Random in-plane rotation
        zrot = rng.random()*360
        atoms.rotate(v='z', a=zrot, center='COU')
        slab.rotate(v='z', a=zrot, center='COU')
        system.rotate(v='z', a=zrot, center='COU') 

        # Cut out atoms outside the cell
        atoms_pos = atoms.positions
        keep = np.ones(len(atoms),bool)
        for j, p in enumerate(atoms_pos):
            if (p[0]>L-limit or p[1]>L-limit or p[2]>thick_choice-limit):
                keep[j] *= False
            elif (p[0]<0+limit or p[1]<0+limit or p[2]<0+limit):
                keep[j] *= False
        atoms = atoms[keep]

        slab_pos = slab.positions
        keep = np.ones(len(slab),bool)
        for j, p in enumerate(slab_pos):
            if (p[0]>L-limit or p[1]>L-limit or p[2]>thick_choice-limit):
                keep[j] *= False
            elif (p[0]<0+limit or p[1]<0+limit or p[2]<0+limit):
                keep[j] *= False
        slab = slab[keep]

        system_pos = system.positions
        keep = np.ones(len(system),bool)
        for j, p in enumerate(system_pos):
            if (p[0]>L-limit or p[1]>L-limit or p[2]>thick_choice-limit):
                keep[j] *= False
            elif (p[0]<0+limit or p[1]<0+limit or p[2]<0+limit):
                keep[j] *= False
        system = system[keep]

        ############################################
        ### Save positions for label generation ####
        ############################################
        ## For multiclass nanoparticle/substrate segmentation
        #positions = np.array([atoms.get_positions()[:,:2], slab.get_positions()[:,:2]],dtype='object')
        
        ## For single class nanoparticle segmentation
        positions = atoms.get_positions()[:,:2]

        ## For single class substrate segmentation
        #positions = slab.get_positions()[:,:2]

        ## For atomic column segmentation
        clusters = fcluster(linkage(positions), 1, criterion='distance')
        unique,indices= np.unique(clusters, return_index=True)
        # Find the positions of the columns and the number of atoms per column.
        sites = np.array([np.mean(positions[clusters==u],axis=0) for u in unique])
        heights = np.array([np.sum(clusters==u) for u in unique])

        ## *** abTEM for TEM image ***
        # Determine number of points for each potential slice
        wave_xy = np.asarray(system.cell[0,0], system.cell[1,1])
        wave_pts = int(wave_xy/sampling)
        
        # Build potential for atoms
        # Number of slices is set by atoms.cell[2,2] with a default thickness of 0.5A
        potential = Potential(system,
                      sampling=sampling,
                      gpts=wave_pts, 
                      parametrization='kirkland', 
                      #slice_thickness=0.5,
                      projection='infinite')
        # Build wave
        wave = PlaneWave(
                energy=300e3     #acceleration voltage in eV
        )
        # Compute exit wave
        exit_wave = wave.multislice(potential,
                                 pbar=False)
        # Save coordinates of positions, sites, and heights
        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                    positions=positions,
                    sites=sites, 
                    heights=heights
                )
        
        # Save wavefunction in hdf5 format along (array, energy, extent)
        exit_wave.write('{0}/wave/wave_{1:04d}'.format(dir_name,i))
        #wave.save('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i))
        ase.io.write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),system)
        print('   Constructed system {:2d} w/ {:5d} atoms'.format(i,
                                                     len(system),
                                                     flush = True))

        ### Log memory usage
        if os.path.exists("memory_usage.txt"):
            mode = 'a' # append if already exists
        else:
            mode = 'w' # make a new file if not
        with open("memory_usage.txt",mode) as text_file:
            print_memory(text_file)

def_resolution=1200
def_sampling=0.05

if __name__ == "__main__":
    # Use the standard argument parser - but also save the names of the positional parameters for
    # creating the reproducibility file.
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'element', 'tilt', 'number')
    parser.add_argument("folder", help="The name of the folder (in /home/niflheim2/mhlla/hrid) where the output is placed.")
    parser.add_argument("element", help="Chemical symbol of the desired material.", type=str)
    parser.add_argument("tilt", help="Maximal off-axis tilt of the nanoparticle", type=float)
    parser.add_argument("number", type=int,
                        help="The desired number of training/testing examples (incl. any that are already done).")
    parser.add_argument("--sampling", type=float, default=def_sampling,
                        help="Sampling of wave function in Angstrom per pixel")
    parser.add_argument("--resolution", type=int, default=def_resolution,
                            help="Resolution of wave function in pixels.")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.numproc > args.number:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('/home/niflheim2/mhlla/hrid', 'simulation_data', args.folder)
    seed = np.random.SeedSequence(args.seed)
    
    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))
    
    print("Generating samples {} to {} in folder {} using {} process(es)\n".format(
        args.start, args.number, dir_name, numproc))
    print("np.random.SeedSequence entropy:", seed.entropy)
    print("\nElement: {}".format(args.element))

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce_supported.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    before = time.time()

    if numproc == 1:
        # Running on a single core.
        driver(first_number=args.start,
                last_number=args.number,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                element=args.element,
                tilt=args.tilt,
                seed=seed)
    else:
        # For abTEM compatability
        multiprocessing.set_start_method('spawn')
        data = []
        ndata = args.number - args.start
        seeds = seed.spawn(numproc)
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number = args.start + (i+1) * ndata // numproc,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                element=args.element,
                tilt=args.tilt,
                seed=seeds[i]
                ))
        with multiprocessing.Pool(numproc) as pool:
            pool.map(driver2, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
